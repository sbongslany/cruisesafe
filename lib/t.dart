// // Main Stateful Widget Start
// import 'package:cruisesafe/ui/views/home/home_tabs/tab_home.dart';
// import 'package:cruisesafe/ui/widget/submit_button.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_svg/svg.dart';
//
// import 'core/config/constants.dart';
//
// class ListViewPage extends StatefulWidget {
//   @override
//   _ListViewPageState createState() => _ListViewPageState();
// }
//
// class _ListViewPageState extends State<ListViewPage> {
//   // Title List Here
//   var titleList = [
//     "Success",
//     "Motivation",
//     "Hard Work",
//     "Decision",
//     "Confidence",
//     "Business",
//     "Team Work"
//   ];
//
//   // Description List Here
//   var descList = [
//     "Push yourself, because no one else is going to do it for you.",
//     "Your limitation—it's only your imagination.",
//     "Hard Work changes the life.",
//     "Sometimes it's the smallest decisions that can change your life forever.",
//     "Confidence is the most beautiful thing you can possess",
//     "A big business starts small.",
//     "Talent wins games, but teamwork and intelligence win championships."
//   ];
//
//   // Image Name List Here
//   var imgList = [
//     "assets/images/img1.jpg",
//     "assets/images/img2.jpg",
//     "assets/images/img3.jpg",
//     "assets/images/img4.jpg",
//     "assets/images/img5.jpg",
//     "assets/images/img6.jpg",
//     "assets/images/img7.jpg"
//   ];
//   var imgList1 = [
//     "test1",
//     "test2",
//     "test3",
//     "test4",
//     "test5",
//     "test6",
//     "test7"
//   ];
//
//   @override
//   Widget build(BuildContext context) {
//     // MediaQuery to get Device Width
//     double width = MediaQuery.of(context).size.width * 0.6;
//     return Scaffold(
//       appBar: AppBar(
//         actions: [
//           IconButton(
//             icon: Icon(Icons.add),
//             onPressed: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                     builder: (context) => showDialogFunc(context)),
//               );
//             },
//           ),
//         ],
//         // App Bar
//         title: Text(
//           "ListView On-Click Event",
//           style: TextStyle(color: Colors.grey),
//         ),
//         elevation: 0,
//         backgroundColor: Colors.white,
//       ),
//       // Main List View With Builder
//       // body: ListView.builder(
//       //   itemCount: imgList.length,
//       //   itemBuilder: (context, index) {
//       //     return GestureDetector(
//       //       onTap: () {
//       //         // This Will Call When User Click On ListView Item
//       //         showDialogFunc(
//       //             context, imgList[index], titleList[index], descList[index]);
//       //       },
//       //       // Card Which Holds Layout Of ListView Item
//       //       child: Card(
//       //         child: Row(
//       //           children: <Widget>[
//       //             Container(
//       //               width: 100,
//       //               height: 100,
//       //               child: Image.asset(imgList[index]),
//       //             ),
//       //             Padding(
//       //               padding: const EdgeInsets.all(10.0),
//       //               child: Column(
//       //                 crossAxisAlignment: CrossAxisAlignment.start,
//       //                 children: <Widget>[
//       //                   Text(
//       //                     titleList[index],
//       //                     style: TextStyle(
//       //                       fontSize: 25,
//       //                       color: Colors.grey,
//       //                       fontWeight: FontWeight.bold,
//       //                     ),
//       //                   ),
//       //                   SizedBox(
//       //                     height: 10,
//       //                   ),
//       //                   Container(
//       //                     width: width,
//       //                     child: Text(
//       //                       descList[index],
//       //                       maxLines: 3,
//       //                       style: TextStyle(
//       //                           fontSize: 15, color: Colors.grey[500]),
//       //                     ),
//       //                   ),
//       //                 ],
//       //               ),
//       //             )
//       //           ],
//       //         ),
//       //       ),
//       //     );
//       //   },
//       // ),
//     );
//   }
// }
//
// // IOS CRUISESAFE DIALOG
// showDialogFunc(context) {
//   var imgList1 = [
//     "assets/svg/A2a.svg",
//     "assets/svg/A2b.svg",
//     "assets/svg/Ak4.svg",
//     "assets/svg/Ak14.svg",
//     "assets/svg/A2b.svg",
//     "assets/svg/A2b.svg"
//   ];
//   return showDialog(
//     //  barrierColor: Colors.grey,
//     context: context,
//     builder: (context) {
//       return Center(
//         child: Material(
//           type: MaterialType.transparency,
//           child: Container(
//             decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(10),
//               color: Colors.white,
//             ),
//             padding: EdgeInsets.all(10),
//             height: 750,
//             width: 380,
//             //width: MediaQuery.of(context).size.width * 0.5,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.center,
//               children: <Widget>[
//                 Row(
//                   children: [],
//                 ),
//                 ClipRRect(
//                   //  borderRadius: BorderRadius.circular(5),
//                   child: Image.asset(
//                     'assets/images/jra_splash2.png',
//                     width: 200,
//                     height: 200,
//                   ),
//                 ),
//
//                 SizedBox(
//                   height: 10,
//                 ),
//                 Expanded(
//                   child: GridView.count(
//                     crossAxisCount: 2,
//                     children: List.generate(
//                         imgList1.length,
//                         (index) => Container(
//                               width: 10,
//                               height: 10,
//                               child: Column(
//                                 children: [
//                                   ClipOval(
//                                     child: SvgPicture.asset(
//                                       imgList1[index],
//                                       width: 50,
//                                       height: 50,
//                                       fit: BoxFit.cover,
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             )
//                         // Text(imgList1[index])
//                         ),
//                   ),
//                 ),
//
//                 SizedBox(
//                   height: 10,
//                 ),
//                 //Button
//                 Container(
//                   height: 50,
//                   width: 200,
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(40),
//                     color: Colors.white,
//                   ),
//                   child: RaisedButton(
//                     onPressed: () {
//                       Navigator.of(context).push(MaterialPageRoute(
//                           builder: (BuildContext context) =>
//                               TabHomeScreen()));
//                 },
//                     child: Text('Cancel'),
//                   ),
//                 ),
//                 SizedBox(
//                   height: 10,
//                 ),
//               ],
//             ),
//           ),
//         ),
//       );
//     },
//   );
// }
