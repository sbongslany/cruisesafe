import 'package:flutter/material.dart';

class Constants {
  static const String USER_TOKEN = "userToken";
  static const String USER_EMAIL = "userEmail";
  static const String USER_FULL_NAME = "userFullName";
  static const String USER_FIRST_NAME = "firstName";
  static const String USER_LAST_NAME = "lastName";
  static const String USER_ID = "userId";
  static const String PROFILE_PICTURE = "profilePicture";
  static const String CURRENT_LATITUDE = "currentLatitude";
  static const String CURRENT_LONGITUDE = "currentLongitude";
  static const String CURRENT_POSITION = "currentPosition";
  static const String USER_MOBILE = "userMobile";
  static const String LOGIN_TYPE = "loginType";

  static const double SPACE_BETWEEN_TXT = 10;
  static const double WIDGET_ALL_SIDES_MARGINS = 10;
  //static Color btnColor = getPrimaryColor();
  static Color btnColor = getPrimaryColor();
  static Color btnBackColor = Colors.white;
  static Color splash = getPrimaryColor();
  static const Color btnTextColor = Colors.black;
  static const Color btnTextColor2 = Colors.white;
  static Color iconColor = getPrimaryColor();
  static Color lblColor = getPrimaryColor();
  static Color cursorColor = getPrimaryColor();
  static Color lblColor2 = Colors.white;
  static Color txtHintColor = Colors.white;
  static Color txtHintLabelColor = Colors.grey;
  static Color txtTextBoxColor = Colors.white;

  static const Color cardColor = Colors.black;
  static const Color cardLabelColor = Colors.white;

  static const Color cardColorW = Colors.white;
  static const Color cardLabelColorW = Colors.black;

  static const TextStyle textStyle = TextStyle(color: Colors.white);

  static  BoxDecoration backGroundDeco({img}) {
    return BoxDecoration(
      color: Colors.black,
      image: DecorationImage(
        image: AssetImage(img ?? "assets/images/home_background.jpeg"),
        fit: BoxFit.cover,
        colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
      ),
    );
  }

  static  BoxDecoration backGroundDecoLighter({img}) {
    return BoxDecoration(
      //color: Colors.black,
      image: DecorationImage(
        image: AssetImage(img ?? "assets/images/home_background.jpeg"),
        fit: BoxFit.cover,
        //colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.dstATop),
      ),
    );
  }

  static const TextStyle txtStyle2 = TextStyle(
    color: Colors.white,
  );

  static const TextStyle txtStyle3 = TextStyle(
      color: Colors.grey,
  );

  //colors
  //https://encycolorpedia.com/adcfe6
  //https://www.colorhexa.com/adcfe6


  static getPrimaryColor() {
    Map<int, Color> color =
    {
      500:Color.fromRGBO(253, 184, 19, 1),
      /**100:Color.fromRGBO(173, 207, 230, 1),
      200:Color.fromRGBO(173, 207, 230, 1),
      300:Color.fromRGBO(173, 207, 230, 1),
      400:Color.fromRGBO(173, 207, 230, 1),
      500:Color.fromRGBO(173, 207, 230, 1),
      600:Color.fromRGBO(173, 207, 230, 1),
      700:Color.fromRGBO(173, 207, 230, 1),
      800:Color.fromRGBO(173, 207, 230, 1),
      900:Color.fromRGBO(173, 207, 230, 1),*/
    };
    MaterialColor primaryColor = MaterialColor(0xFFFDB813, color); //#fdb813
    return primaryColor;
  }

  static getPrimaryColorTheme() {
    Map<int, Color> color =
    {
      50:Color.fromRGBO(173, 207, 230, 1),
      100:Color.fromRGBO(173, 207, 230, 1),
      200:Color.fromRGBO(173, 207, 230, 1),
      300:Color.fromRGBO(173, 207, 230, 1),
      400:Color.fromRGBO(173, 207, 230, 1),
      500:Color.fromRGBO(173, 207, 230, 1),
      600:Color.fromRGBO(173, 207, 230, 1),
      700:Color.fromRGBO(173, 207, 230, 1),
      800:Color.fromRGBO(173, 207, 230, 1),
      900:Color.fromRGBO(173, 207, 230, 1),
    };
    MaterialColor primaryColor = MaterialColor(0xFF000000, color);
    return primaryColor;
  }

}