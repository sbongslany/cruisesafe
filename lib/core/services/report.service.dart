import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/core.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/ui/shared/headers.dart';
import 'package:cruisesafe/ui/widget/progressLoaderDialog.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class ReportService {

  //Get new reports
  Future<http.Response?> getMyReports({context}) async {
    try {
      String userId = await MySharedPreference.get(Constants.USER_ID);
      //showLoaderDialog(context: context, loadingMessage: "Trying to register...");
      String url = '${Config.api}/api/reporting/getMyReport?reporting_registered_id=$userId&reporting_client_id=1';
      print(url);
      final response = await http.get(Uri.parse(url), headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally{
      //Navigator.pop(context);//clear progress bar
    }
  }

  //Get new reports
  Future<http.Response?> getReportsAroundYou({context}) async {
    try {
      Position position = await CruiseSafeLocation.getGeoLocationPosition();

      String userId = MySharedPreference.get(Constants.USER_ID).toString();
      //showLoaderDialog(context: context, loadingMessage: "Checking around you...");
      String url = '${Config.api}/api/reporting/getReportTicketsAround?client_id=1&latitude=${position.latitude}&longitude=${position.longitude}&distance=50';
      print(url);
      final response = await http.get(Uri.parse(url), headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally{
      //Navigator.pop(context);//clear progress bar
    }
  }

  //Get Categories
  Future<http.Response?> getCategories({context}) async {
    try {
      String userId = await MySharedPreference.get(Constants.USER_ID);
      //showLoaderDialog(context: context, loadingMessage: "Checking around you...");
      String url = '${Config.api}/api/reporting/getCategories?client_id=1';
      print(url);
      final response = await http.get(Uri.parse(url), headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally{
      //Navigator.pop(context);//clear progress bar
    }
  }

  //Get Categories
  Future<http.Response?> getFAQ({context}) async {
    try {
      Position position = await CruiseSafeLocation.getGeoLocationPosition();

      String userId = await MySharedPreference.get(Constants.USER_ID);
      //showLoaderDialog(context: context, loadingMessage: "Checking around you...");
      String url = '${Config.api}/api/reporting/getFaqs?faq_client_id=1';
      print(url);
      final response = await http.get(Uri.parse(url), headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally{
      //Navigator.pop(context);//clear progress bar
    }
  }

  //Save report
  Future<http.Response?> saveReport({
    context,
    latitude,
    longitude,
    personReporting,
    userId,
    reportingType,
    comment,
    personLocationLatLong,
    fullAddress,
    postalCode,
    province,
    locationType,
    street,
    suburb,
    town,
    metropolitan,
    country,
    reporting_category_id,
    reporting_category_sub_id,
    category,
    subCategory,
    reporting_title
  }) async {
    try {
      showLoaderDialog(context: context, loadingMessage: "Submitting...");
      String url = '${Config.api}/api/reporting/saveReport';
      print(url);
      final body = jsonEncode(
          {
            "image":[

            ],
            "report_it_comment":[

            ],
            "report_itt":{
              "reporting_category_id": reporting_category_id ?? 0,
              "reporting_category_sub_id": reporting_category_sub_id ?? 0,
              "reporting_app_record_id":1,
              "reporting_app_status":"new",
              "reporting_app_status_date":"",
              "reporting_as_anonymous":"",
              "reporting_body": comment ?? "",
              "reporting_category": reportingType ?? "",
              "reporting_client_id":1,
              "reporting_comfirmed_by":0,
              "reporting_comfirmed_date":"",
              "reporting_comfirmer_comment":"",
              "reporting_day":0,
              "reporting_delete_comment":"",
              "reporting_delete_date":"",
              "reporting_deleted_by":0,
              "reporting_device_id":"",
              "reporting_device_name":"",
              "reporting_device_type":"iOS",
              "reporting_id":0,
              "reporting_incident_location_address": fullAddress ?? "",
              "reporting_incident_location_country":country ?? "",
              "reporting_incident_location_end_latitude":"",
              "reporting_incident_location_end_longitude":"",
              "reporting_incident_location_latitude":latitude ?? "0.0",
              "reporting_incident_location_longitude":longitude ?? "0.0",
              "reporting_incident_location_metropolitan":metropolitan ?? "",
              "reporting_incident_location_postal_code": postalCode ?? "",
              "reporting_incident_location_province": province ?? "",
              "reporting_incident_location_street":street ?? "",
              "reporting_incident_location_suburb":suburb ?? "",
              "reporting_incident_location_town":town ?? "",
              "reporting_incident_location_type":locationType ?? "Map",
              "reporting_last_update":"",
              "reporting_month":0,
              "reporting_other":"",
              "reporting_person_location": personLocationLatLong ?? "",
              "reporting_person_name": personReporting ?? "",
              "reporting_reference":"",
              "reporting_registered_id": userId ?? "",
              "reporting_resolved_by":0,
              "reporting_resolved_date":"",
              "reporting_resolver_comment":"",
              "reporting_status":"new",
              "reporting_sub_category": subCategory ?? "",
              "reporting_system_reporting_id":0,
              "reporting_ticket_id":0,
              "reporting_ticket_reference":"",
              "reporting_timestamp":"2021-10-25 00:00:11",
              "reporting_title": reporting_title ?? "",
              "reporting_type":"JRA-Fixit",
              "reporting_verified_by":0,
              "reporting_verified_comment":"",
              "reporting_verified_date":"",
              "reporting_viewed_by":0,
              "reporting_viewed_date":"",
              "reporting_viewer_comment":"",
              "reporting_year":0
            },
            "voice":[

            ]
          }
      );

      print(body);
      final response = await http.post(Uri.parse(url), body: body, headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      return null;
    }finally {
      Navigator.pop(context);//clear progress bar
    }
  }
}