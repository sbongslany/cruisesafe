import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/core.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/ui/shared/headers.dart';
import 'package:cruisesafe/ui/widget/progressLoaderDialog.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class UserService {

  //Registration
  Future<http.Response?> signUp({context, registered_twitter_id, registered_facebook_id, registered_login_with, photoUrl, google_id, firstName, surname, email, mobile, password, userId, isSocialMedia}) async {
    try {
      showLoaderDialog(context: context, loadingMessage: "Trying to update user...");
      String url = '${Config.api}/api/reporting/registerUserAccount';

      if(isSocialMedia != null && isSocialMedia) {
        url = '${Config.api}/api/reporting/registerUser';
      }

      print(url);
      final body = jsonEncode({
        "registered_account_image": "",
        "registered_address": "",
        "registered_address_code": "",
        "registered_address_coordinates": "",
        "registered_address_latitude": "",
        "registered_address_longitude": "",
        "registered_address_municipality": "",
        "registered_address_provice": "",
        "registered_address_region": "",
        "registered_address_street": "",
        "registered_address_suburb": "",
        "registered_address_town": "",
        "registered_bluetooth_address": "",
        "registered_bluetooth_allowed": "yes",
        "registered_bluetooth_extra": "",
        "registered_cellphone": "",
        "registered_client_id": 1,
        "registered_coordinates": "",
        "registered_count": 0,
        "registered_data_access_allowed": "yes",
        "registered_date": "2021-12-16T00:00:00",
        "registered_description": "",
        "registered_device_id": "",
        "registered_device_type": "",
        "registered_email": email ?? "",
        "registered_event_pothole": "yes",
        "registered_event_road_condition": "yes",
        "registered_event_traffic_ligth": "yes",
        "registered_expiry_date": "",
        "registered_facebook_id": registered_facebook_id ?? "",
        "registered_facebook_image": "",
        "registered_google_id": google_id ?? "",
        "registered_google_image": photoUrl ?? "",
        "registered_id": userId ?? "",
        "registered_identified_count": 0,
        "registered_last_modified_date": "",
        "registered_latitude": "",
        "registered_location": "",
        "registered_location_allowed": "yes",
        "registered_login_from": "IOS",
        "registered_login_with": registered_login_with ?? "Account",
        "registered_longitude": "",
        "registered_map_type": "Satellite",
        "registered_mobile_number": mobile ?? "",
        "registered_name": firstName ?? "",
        "registered_notification_allowed": "yes",
        "registered_otp": "",
        "registered_otp_entered": "no",
        "registered_password_temp": "",
        "registered_password_temp_expiry_date": "",
        "registered_password_temp_status": "",
        "registered_postal_code": "",
        "registered_province": "",
        "registered_safe_driving_auto_dectation": "yes",
        "registered_safe_driving_auto_start": "yes",
        "registered_safe_driving_popup_confirmation": "yes",
        "registered_safe_driving_popup_notification": "yes",
        "registered_safe_driving_quick_reporting": "yes",
        "registered_safe_driving_voice_confirmation": "yes",
        "registered_safe_driving_voice_notification": "yes",
        "registered_status": "new",
        "registered_surburb": "",
        "registered_surname": surname ?? "",
        "registered_password": password ?? "",
        "registered_timestamp": "",
        "registered_title": "",
        "registered_town": "",
        "registered_twitter_createdAt": "",
        "registered_twitter_defaultProfile": "",
        "registered_twitter_favouritesCount": 0,
        "registered_twitter_followersCount": 0,
        "registered_twitter_friendsCount": 0,
        "registered_twitter_id": registered_twitter_id ?? "",
        "registered_twitter_location": "",
        "registered_twitter_profileImageUrl": "",
        "registered_twitter_profileImageUrlHttps": "",
        "registered_twitter_screenName": "",
        "registered_twitter_statusesCount": 0,
        "registered_twitter_url": "",
        "registered_twitter_verified": "",
        "registered_updated_by": 0,
        "registered_username": email ?? ""
      });

      print(body);

      final response = await http.post(Uri.parse(url), body: body, headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally {
      Navigator.pop(context);//clear progress bar
    }
  }

  //Registration
  Future<http.Response?> updateUser({context, firstName, surname, email, mobile, userId}) async {
    try {
      showLoaderDialog(context: context, loadingMessage: "Updating user...");
      String url = '${Config.api}/api/reporting/updateUser';
      print(url);
      final body = jsonEncode({
        "registered_account_image": "profile_picture",
        "registered_address": "",
        "registered_address_code": "",
        "registered_address_coordinates": "",
        "registered_address_latitude": "",
        "registered_address_longitude": "",
        "registered_address_municipality": "",
        "registered_address_provice": "",
        "registered_address_region": "",
        "registered_address_street": "",
        "registered_address_suburb": "",
        "registered_address_town": "",
        "registered_bluetooth_address": "",
        "registered_bluetooth_allowed": "yes",
        "registered_bluetooth_extra": "",
        "registered_cellphone": "",
        "registered_client_id": 1,
        "registered_coordinates": "",
        "registered_count": 0,
        "registered_data_access_allowed": "yes",
        "registered_date": "2021-12-16T00:00:00",
        "registered_description": "",
        "registered_device_id": "",
        "registered_device_type": "",
        "registered_email": email,
        "registered_event_pothole": "yes",
        "registered_event_road_condition": "yes",
        "registered_event_traffic_ligth": "yes",
        "registered_expiry_date": "",
        "registered_facebook_id": "",
        "registered_facebook_image": "",
        "registered_google_id": "",
        "registered_google_image": "",
        "registered_id": userId,
        "registered_identified_count": 0,
        "registered_last_modified_date": "",
        "registered_latitude": "",
        "registered_location": "",
        "registered_location_allowed": "yes",
        "registered_login_from": "IOS",
        "registered_login_with": "Account",
        "registered_longitude": "",
        "registered_map_type": "Satellite",
        "registered_mobile_number": mobile ?? "",
        "registered_name": firstName ?? "",
        "registered_notification_allowed": "yes",
        "registered_otp": "",
        "registered_otp_entered": "no",
        "registered_password_temp": "",
        "registered_password_temp_expiry_date": "",
        "registered_password_temp_status": "",
        "registered_postal_code": "",
        "registered_province": "",
        "registered_safe_driving_auto_dectation": "yes",
        "registered_safe_driving_auto_start": "yes",
        "registered_safe_driving_popup_confirmation": "yes",
        "registered_safe_driving_popup_notification": "yes",
        "registered_safe_driving_quick_reporting": "yes",
        "registered_safe_driving_voice_confirmation": "yes",
        "registered_safe_driving_voice_notification": "yes",
        "registered_status": "new",
        "registered_surburb": "",
        "registered_surname": surname,
        "registered_timestamp": "",
        "registered_title": "",
        "registered_town": "",
        "registered_twitter_createdAt": "",
        "registered_twitter_defaultProfile": "",
        "registered_twitter_favouritesCount": 0,
        "registered_twitter_followersCount": 0,
        "registered_twitter_friendsCount": 0,
        "registered_twitter_id": "",
        "registered_twitter_location": "",
        "registered_twitter_profileImageUrl": "",
        "registered_twitter_profileImageUrlHttps": "",
        "registered_twitter_screenName": "",
        "registered_twitter_statusesCount": 0,
        "registered_twitter_url": "",
        "registered_twitter_verified": "",
        "registered_updated_by": 0,
        "registered_username": email
      });
      final response = await http.post(Uri.parse(url), body: body, headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally {
      Navigator.pop(context);//clear progress bar
    }
  }

  //Registration
  Future<http.Response?> signIn({context, firstName, surname, email, mobile, password}) async {
    try {
      showLoaderDialog(context: context, loadingMessage: "Trying to login...");
      String url = '${Config.api}/api/reporting/registerUserLogin';
      print(url);
      print(password);
      final body = jsonEncode({
        "registered_account_image": "profile_picture",
        "registered_address": "",
        "registered_address_code": "",
        "registered_address_coordinates": "",
        "registered_address_latitude": "",
        "registered_address_longitude": "",
        "registered_address_municipality": "",
        "registered_address_provice": "",
        "registered_address_region": "",
        "registered_address_street": "",
        "registered_address_suburb": "",
        "registered_address_town": "",
        "registered_bluetooth_address": "",
        "registered_bluetooth_allowed": "yes",
        "registered_bluetooth_extra": "",
        "registered_cellphone": "",
        "registered_client_id": 1,
        "registered_coordinates": "",
        "registered_count": 0,
        "registered_data_access_allowed": "yes",
        "registered_date": "2021-12-16T00:00:00",
        "registered_description": "",
        "registered_device_id": "",
        "registered_device_type": "",
        "registered_email": email,
        "registered_event_pothole": "yes",
        "registered_event_road_condition": "yes",
        "registered_event_traffic_ligth": "yes",
        "registered_expiry_date": "",
        "registered_facebook_id": "",
        "registered_facebook_image": "",
        "registered_google_id": "",
        "registered_google_image": "",
        "registered_id": 0,
        "registered_identified_count": 0,
        "registered_last_modified_date": "",
        "registered_latitude": "",
        "registered_location": "",
        "registered_location_allowed": "yes",
        "registered_login_from": "IOS",
        "registered_login_with": "Account",
        "registered_longitude": "",
        "registered_map_type": "Satellite",
        "registered_mobile_number": "0784884519",
        "registered_name": "Mncedicy",
        "registered_notification_allowed": "yes",
        "registered_otp": "",
        "registered_otp_entered": "no",
        "registered_password": password ?? "",
        "registered_password_temp": "",
        "registered_password_temp_expiry_date": "",
        "registered_password_temp_status": "",
        "registered_postal_code": "",
        "registered_province": "",
        "registered_safe_driving_auto_dectation": "yes",
        "registered_safe_driving_auto_start": "yes",
        "registered_safe_driving_popup_confirmation": "yes",
        "registered_safe_driving_popup_notification": "yes",
        "registered_safe_driving_quick_reporting": "yes",
        "registered_safe_driving_voice_confirmation": "yes",
        "registered_safe_driving_voice_notification": "yes",
        "registered_status": "new",
        "registered_surburb": "",
        "registered_surname": surname,
        "registered_timestamp": "",
        "registered_title": "",
        "registered_town": "",
        "registered_twitter_createdAt": "",
        "registered_twitter_defaultProfile": "",
        "registered_twitter_favouritesCount": 0,
        "registered_twitter_followersCount": 0,
        "registered_twitter_friendsCount": 0,
        "registered_twitter_id": "",
        "registered_twitter_location": "",
        "registered_twitter_profileImageUrl": "",
        "registered_twitter_profileImageUrlHttps": "",
        "registered_twitter_screenName": "",
        "registered_twitter_statusesCount": 0,
        "registered_twitter_url": "",
        "registered_twitter_verified": "",
        "registered_updated_by": 0,
        "registered_username": email
      });
      final response = await http.post(Uri.parse(url), body: body, headers: AppHeaders.unAuthenticatedHeaders());
      return response;
    }catch(e){
      print(e);
      return null;
    }finally {
      Navigator.pop(context);//clear progress bar
    }
  }
}