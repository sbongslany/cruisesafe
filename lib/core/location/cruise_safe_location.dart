import 'dart:io';

import 'package:geolocator/geolocator.dart';
import 'package:geocoding/geocoding.dart';

/// Reference
/// https://protocoderspoint.com/flutter-get-current-location-address-from-latitude-longitude/

class CruiseSafeLocation {

  static Future<Position?> reloadGeoLocationPosition() async {
    print('start getting location ' + DateTime.now().toString());
    int start = DateTime.now().second;
    try {
      bool serviceEnabled;
      LocationPermission permission;

      // Test if location services are enabled.
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        // Location services are not enabled don't continue
        // accessing the position and request users of the
        // App to enable the location services.
        await Geolocator.openLocationSettings();
        return Future.error('Location services are disabled.');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }
      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      return await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
          timeLimit: Duration(seconds: 30)
      );
    }on SocketException catch (e) {
      print('No Internet: ' + e.toString());
      return null;
    }catch(e){
      return null;
    }finally{
      int end = DateTime.now().second;
      print('done getting location ' + DateTime.now().toString());
      print('total: ' + (end - start).toString());
    }
  }

  static Future<Position> getGeoLocationPosition() async {
    print('start getting location ' + DateTime.now().toString());
    int start = DateTime.now().second;
    try {
      bool serviceEnabled;
      LocationPermission permission;

      // Test if location services are enabled.
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        // Location services are not enabled don't continue
        // accessing the position and request users of the
        // App to enable the location services.
        await Geolocator.openLocationSettings();
        return Future.error('Location services are disabled.');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          return Future.error('Location permissions are denied');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      Position? pos = await Geolocator.getLastKnownPosition();
      if(pos != null) {
        return pos;
      }

      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      return await Geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.high,
          timeLimit: Duration(seconds: 30)
      );
    }finally{
      int end = DateTime.now().second;
      print('done getting location ' + DateTime.now().toString());
      print('total: ' + (end - start).toString());
    }
  }

  static Future getAddressFromPosition(Position position)async {
    List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    //address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    return '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
  }

  static Future<Placemark> getPlaceMarkFromPosition(Position position)async {
    List<Placemark> placemarks = await placemarkFromCoordinates(position.latitude, position.longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    //address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    return place;
  }

  static Future getAddressFromLatLng(double latitude, double longitude)async {
    List<Placemark> placemarks = await placemarkFromCoordinates(latitude, longitude);
    print(placemarks);
    Placemark place = placemarks[0];
    //address = '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    return '${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
  }



  static Future<double> getLatitude() async {
    Position position = await getGeoLocationPosition();
    print('latitude: ' + position.latitude.toString());
    return position.latitude;
  }

  static Future<double> getLongitude() async {
    Position position = await getGeoLocationPosition();
    print('longitude: ' + position.longitude.toString());
    return position.longitude;
  }

}