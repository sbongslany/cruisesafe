import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/t.dart';
import 'package:cruisesafe/ui/router.dart';
import 'package:flutter/material.dart';

//import 'core/t.dart';

import 'ui/router.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'CruiseSafe',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        //primarySwatch: Constants.getPrimaryColor(),
        primarySwatch: Constants.getPrimaryColorTheme(),
      ),
      //home: ListViewPage(),
      initialRoute: Routers.startUp,
      onGenerateRoute: Routers.generateRoute,
    );
  }
}
