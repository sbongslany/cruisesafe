import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/services/user.service.dart';
import 'package:cruisesafe/ui/shared/theme.helper.dart';
import 'package:cruisesafe/ui/views/home/home_screen.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:cruisesafe/ui/widget/submit_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class UpdateProfile extends StatefulWidget {
  const UpdateProfile({Key? key}) : super(key: key);

  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {

  TextEditingController _txtNameController = new TextEditingController();
  TextEditingController _txtSurnameController = new TextEditingController();
  TextEditingController _txtMobileController = new TextEditingController();
  TextEditingController _txtEmailController = new TextEditingController();
  TextEditingController _txtPassword1Controller = new TextEditingController();
  TextEditingController _txtPassword2Controller = new TextEditingController();
  TextEditingController _txtAddressController = new TextEditingController();
  String userId = "";

  bool checkboxValue = false;

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  _loadProfile() async{
    _txtNameController.text = await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "";
    _txtSurnameController.text = await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "";
    _txtMobileController.text = await MySharedPreference.get(Constants.USER_MOBILE) ?? "";
    _txtEmailController.text = await MySharedPreference.get(Constants.USER_EMAIL) ?? "";
    String _userId = await MySharedPreference.get(Constants.USER_ID) ?? "";
    setState(() {
      userId = _userId;
    });
    print(_userId);
  }

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /**appBar: AppBar(
          title: Text("Register"),
          ),*/
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            decoration: Constants.backGroundDeco(),
            child: Column(
              children: [
                SizedBox(height: 10,),
                //ListTile(leading: IconButton(icon: Icon(Icons.arrow_back_outlined), onPressed: () {Navigator.pop(context); },),),
                Container(
                    margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          SizedBox(height: 50,),
                          Text('UPDATE PROFILE', style: TextStyle(color: Constants.lblColor2, fontSize: 25, decoration: TextDecoration.underline,),),
                          SizedBox(height: 30,),
                          txtNameWidget(),
                          SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          txtSurnameWidget(),
                          SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          txtMobileWidget(),
                          SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          txtEmailWidget(),
                          SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          //txtPassword1Widget(),
                          //SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          //txtPassword2Widget(),
                          //SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          //txtAddressWidget(),
                          SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              ElevatedButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text("CANCEL", style: TextStyle(color: Constants.btnTextColor2),),
                              style: ElevatedButton.styleFrom(
                                //shape: StadiumBorder(),
                                primary: Constants.btnColor,
                                minimumSize: Size(MediaQuery.of(context).size.width * 0.47, 50), // double.infinity is the width and 30 is the height
                                ),
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  submit();
                                },
                                child: Text("SUBMIT", style: TextStyle(color: Constants.btnTextColor2),),
                                style: ElevatedButton.styleFrom(
                                  //shape: StadiumBorder(),
                                  primary: Constants.btnColor,
                                    minimumSize: Size(MediaQuery.of(context).size.width * 0.47, 50), // double.infinity is the width and 30 is the height
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        )
    );
  }

  submit() async {

    if (!_formKey.currentState!.validate()) {
      return;
    }

    /**if(!checkboxValue){
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Accept Terms & Conditions");
      return;
    }*/

    http.Response? res = await UserService().updateUser(
        context: context,
        firstName: _txtNameController.text,
        surname: _txtSurnameController.text,
        email: _txtEmailController.text,
        mobile: _txtMobileController.text,
        userId: userId
    );

    if(res == null) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
      return;
    }

    print(res.body);
    print(res.statusCode);
    var results = jsonDecode(res.body);

    if(res.statusCode == 201) {
      //print(res.body);
      //SnackBarWidget.showSnackBar(color: Colors.blueGrey, context: context, message: results['message']);
      var logType = await MySharedPreference.get(Constants.LOGIN_TYPE);
      var profPicture = await MySharedPreference.get(Constants.PROFILE_PICTURE);
      MySharedPreference.clear(); //clear all local storage
      await MySharedPreference.save(key: Constants.USER_EMAIL, value: _txtEmailController.text);
      await MySharedPreference.save(key: Constants.USER_FULL_NAME, value: _txtNameController.text + " " + _txtSurnameController.text);
      await MySharedPreference.save(key: Constants.USER_FIRST_NAME, value: _txtNameController.text);
      await MySharedPreference.save(key: Constants.USER_LAST_NAME, value: _txtSurnameController.text);
      await MySharedPreference.save(key: Constants.USER_MOBILE, value: _txtMobileController.text);
      await MySharedPreference.save(key: Constants.USER_ID, value: userId);
      await MySharedPreference.save(key: Constants.LOGIN_TYPE, value: logType);
      await MySharedPreference.save(key: Constants.PROFILE_PICTURE, value: profPicture);
      Navigator.pop(context);
    }else if(res.statusCode == 400) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else if(res.statusCode == 409) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else{
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
    }
  }

  Widget txtNameWidget() {
    return TextFormField(
      style: TextStyle(color: Constants.txtTextBoxColor),
      cursorColor: Constants.cursorColor,
      controller: _txtNameController,
      decoration: ThemeHelper().textInputDecoration2('First Name', 'Enter your name'),
      validator: (val) {
        if (val!.length == 0) {
          return "Name cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtSurnameWidget() {
    return TextFormField(
      style: TextStyle(color: Constants.txtTextBoxColor),
      cursorColor: Constants.cursorColor,
      controller: _txtSurnameController,
      decoration: ThemeHelper().textInputDecoration2('Last Name', 'Enter your last name'),
      validator: (val) {
        if (val!.length == 0) {
          return "Surname cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtMobileWidget() {
    return TextFormField(
      style: TextStyle(color: Constants.txtTextBoxColor),
      cursorColor: Constants.cursorColor,
      controller: _txtMobileController,
      keyboardType: TextInputType.number,
      decoration: ThemeHelper().textInputDecoration2('Mobile Phone Number', 'Enter your mobile number'),
      validator: (val) {
        if (val!.length == 0) {
          return "Mobile number cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtPassword1Widget() {
    return TextFormField(
      controller: _txtPassword1Controller,
      obscureText: true,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration('PASSWORD', 'Enter your password'),
      validator: (val) {
        if (val!.length == 0) {
          return "Password cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtPassword2Widget() {
    return TextFormField(
      controller: _txtPassword2Controller,
      obscureText: true,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration('PASSWORD', 'Enter your password'),
      validator: (val) {
        if (val!.length == 0) {
          return "Password cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtEmailWidget() {
    return TextFormField(
      controller: _txtEmailController,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration2('EMAIL ADDRESS/PHONE NUMBER', 'Enter email address or phone number'),
      validator: (val) {
        String pattern =
            r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
            r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
            r"{0,253}[a-zA-Z0-9])?)*$";
        RegExp regex = RegExp(pattern);
        if (val!.length == 0) {
          return "Email address cannot be empty";
        }else if (!regex.hasMatch(val)) {
          return "Invalid email address";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtAddressWidget() {
    return TextFormField(
      controller: _txtAddressController,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration('ADDRESS', 'Enter address'),
      validator: (val) {
        if (val!.length == 0) {
          return "Address cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget termsAndCondition() {
    return  Row(
      children: <Widget>[
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: Checkbox(
              value: checkboxValue,
              checkColor: Constants.txtTextBoxColor,
              activeColor: Constants.lblColor,
              onChanged: (value) {
                setState(() {
                  checkboxValue = value!;
                });
              }),
        ),
        Text(
          "Agree To Terms and Conditions",
          style: TextStyle(color: Constants.btnColor, fontSize: 17, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

}
