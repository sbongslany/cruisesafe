import 'dart:ffi';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/ui/shared/theme.helper.dart';
import 'package:cruisesafe/ui/views/home/home_tabs/tab_support.dart';
import 'package:cruisesafe/ui/views/reports/jra_report_list.dart';
import 'package:cruisesafe/ui/views/reports/reported_issues_google_map_screen.dart';
import 'package:cruisesafe/ui/views/user/update_profile.dart';
import 'package:cruisesafe/ui/widget/top_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:ui' as ui; // Add this line

class UserProfile extends StatefulWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {
  String firstName = "";
  String lastName = "";
  String mobile = "";
  String email = "";
  String loginType = "";

  TextEditingController _txtNameController = new TextEditingController();
  TextEditingController _txtSurnameController = new TextEditingController();
  TextEditingController _txtMobileController = new TextEditingController();
  TextEditingController _txtEmailController = new TextEditingController();

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  final ImagePicker _picker = ImagePicker();
  List<XFile>? _imageFileList = [];
  String profilePicture = "";

  void _loadUserProfile() async {
    String fN = await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "";
    String sN = await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "";
    String pM = await MySharedPreference.get(Constants.USER_MOBILE) ?? "";
    String eM = await MySharedPreference.get(Constants.USER_EMAIL) ?? "";
    String lT = await MySharedPreference.get(Constants.LOGIN_TYPE) ?? "";
    String pp = await MySharedPreference.get(Constants.PROFILE_PICTURE) ?? "";

    _txtMobileController.text = pM;
    _txtSurnameController.text = sN;
    _txtNameController.text = fN;
    _txtEmailController.text = eM;

    setState(() {
      firstName = fN;
      lastName = sN;
      mobile = pM;
      email = eM;
      loginType = lT;
      profilePicture = pp;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadUserProfile();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: Constants.backGroundDeco(),
          child: Container(
            //color: Constants.splash,
            margin: EdgeInsets.only(left: 10, right: 10),
            child: ListView(
              children: [
                SizedBox(height: 10,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pop(context);
                      }, icon: Icon(Icons.arrow_back_outlined, color: Colors.white,),
                    ),
                    IconButton(
                      onPressed: () async {
                        //_buildUpdateUserProfilePopupDialog(context);
                        await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => UpdateProfile()));
                        _loadUserProfile();
                      }, icon: Icon(Icons.edit, color: Colors.white,),
                    ),
                  ],
                ),
                roundProfile(),
                Center(child: Text(firstName + ' ' + lastName, style: TextStyle(color: Colors.white, fontSize: 20),)),
                Center(child: Text(_txtEmailController.text, style: TextStyle(color: Colors.white),)),
                SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                /**SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    roundButton1(
                      title: 'REPORTS',
                      onTap: () {
                        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => JraReportList(fromHome: false, detailedReporting: false,)));
                      }
                    ),
                    roundButton1(
                      icon: Icons.help_outline_outlined,
                      title: 'SUPPORT',
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (BuildContext context) => TabSupportScreen(fromSupport: true,)));
                      }
                    ),
                    roundButton1(
                        icon: Icons.warning,
                        title: 'STATUS',
                        onTap: () {
                          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => ReportedIssuesGoogleMapScreen()));
                        }
                    ),
                  ],
                ),*/
                SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                ListTile(
                  minLeadingWidth: 0,
                  leading: Icon(Icons.person_outline, color: Constants.iconColor,),
                  title: Text('Last Name', style: Constants.txtStyle3),
                  subtitle: Text(lastName, style: Constants.txtStyle2),
                ),
                Divider(color: Colors.white,),
                ListTile(
                  minLeadingWidth: 0,
                  leading: Icon(Icons.person_outline, color: Constants.iconColor,),
                  title: Text('First Name', style: Constants.txtStyle3),
                  subtitle: Text(firstName, style: Constants.txtStyle2),
                ),
                Divider(color: Colors.white,),
                ListTile(
                  minLeadingWidth: 0,
                  leading: Icon(Icons.phone_android_outlined, color: Constants.iconColor,),
                  title: Text('Phone Number', style: Constants.txtStyle3),
                  subtitle: Text(mobile, style: Constants.txtStyle2),
                ),
                Divider(color: Colors.white,),
                ListTile(
                  minLeadingWidth: 0,
                  leading: Icon(Icons.mail_outline, color: Constants.iconColor,),
                  title: Text('Email Address', style: Constants.txtStyle3),
                  subtitle: Text(email, style: Constants.txtStyle2),
                ),
                Divider(color: Colors.white,),
                ListTile(
                  minLeadingWidth: 0,
                  leading: Icon(Icons.location_on_outlined, color: Constants.iconColor,),
                  title: Text('Logged in with', style: Constants.txtStyle3),
                  subtitle: Text(loginType, style: Constants.txtStyle2,),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget roundButton1({icon, title, onTap}) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          decoration: BoxDecoration(
              color: Constants.iconColor,
              border: Border.all(
                color: Colors.black,
              ),
              borderRadius: BorderRadius.all(Radius.circular(100))
          ),
          width: 100,
          height: 100,
          child: Column(
            children: [
              SizedBox(height: 30,),
              Icon(icon ?? Icons.description_outlined),
              //SizedBox(height: 10,),
              Text(title ?? 'Reports', style: TextStyle(color: Colors.black),)
            ],
          )
      ),
    );
  }

  Widget roundButton2() {
    return Positioned(
      top: 217,
      left: 140,
      child: Container(
          decoration: BoxDecoration(
              color: Colors.black,
              border: Border.all(
                color: Colors.black,
              ),
              borderRadius: BorderRadius.all(Radius.circular(100))
          ),
          width: 100,
          height: 100,
          child: Column(
            children: [
              SizedBox(height: 30,),
              Text('17', style: TextStyle(color: Colors.white),),
              SizedBox(height: 10,),
              Text('Support', style: TextStyle(color: Colors.white),)
            ],
          )
      ),
    );
  }

  Widget roundButton3() {
    return Positioned(
      top: 217,
      right: 10,
      child: Container(
          decoration: BoxDecoration(
              color: Colors.black,
              border: Border.all(
                color: Colors.black,
              ),
              borderRadius: BorderRadius.all(Radius.circular(100))
          ),
          width: 100,
          height: 100,
          child: Column(
            children: [
              SizedBox(height: 30,),
              Text('17', style: TextStyle(color: Colors.white),),
              SizedBox(height: 10,),
              Text('Notifications', style: TextStyle(color: Colors.white),)
            ],
          )
      ),
    );
  }

  Widget roundProfile() {
    return GestureDetector(
      onTap: () {
        _buildCameraPopupDialog(context);
      },
      child: Center(
        child: Container(
            decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(
                  color: Colors.black,
                ),
                borderRadius: BorderRadius.all(Radius.circular(100))
            ),
            width: 130,
            height: 130,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(100.0),
              child: Image.asset(profilePicture == "" ? "assets/images/prof-pic.jpeg" : profilePicture, fit: BoxFit.cover, width: MediaQuery.of(context).size.width * 0.29,),
            )
        ),
      ),
    );
  }

  _buildUpdateUserProfilePopupDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("Choose"),
      content: Container(
        height: MediaQuery.of(context).size.height * 0.7,
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              Image.asset('assets/images/jra_splash2.png', width: MediaQuery.of(context).size.width * 0.4,),
              txtNameWidget(),
              SizedBox(height: 20,),
              txtSurnameWidget(),
              SizedBox(height: 20,),
              txtMobileWidget(),
              SizedBox(height: 20,),
              txtEmailWidget(),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  ElevatedButton(
                    onPressed: () { Navigator.pop(context); },
                    child: Text('CANCEL', style: TextStyle(color: Constants.btnTextColor),),
                    style: ElevatedButton.styleFrom(
                      primary: Constants.btnColor,
                      minimumSize: ui.Size(MediaQuery.of(context).size.width * 0.31, MediaQuery.of(context).size.height * 0.04), // double.infinity is the width and 30 is the height
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () { _submit(); },
                    child: Text('SUBMIT', style: TextStyle(color: Constants.btnTextColor),),
                    style: ElevatedButton.styleFrom(
                      primary: Constants.btnColor,
                      minimumSize: ui.Size(MediaQuery.of(context).size.width * 0.31, MediaQuery.of(context).size.height * 0.04), // double.infinity is the width and 30 is the height
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Widget txtNameWidget() {
    return TextFormField(
      controller: _txtNameController,
      decoration: ThemeHelper().textInputDecoration('Name', 'Enter your name'),
      validator: (val) {
        if (val!.length == 0) {
          return "Name cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtSurnameWidget() {
    return TextFormField(
      controller: _txtSurnameController,
      decoration: ThemeHelper().textInputDecoration('Surname', 'Enter your surname'),
      validator: (val) {
        if (val!.length == 0) {
          return "Surname cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtMobileWidget() {
    return TextFormField(
      controller: _txtMobileController,
      decoration: ThemeHelper().textInputDecoration('Mobile Number', 'Enter your mobile number'),
      validator: (val) {
        if (val!.length == 0) {
          return "Mobile number cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtEmailWidget() {
    return TextFormField(
      controller: _txtEmailController,
      decoration: ThemeHelper().textInputDecoration('Email Address', 'Enter your email address'),
      validator: (val) {
        if (val!.length == 0) {
          return "Email address cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  _submit() async {
    if (!_formKey.currentState!.validate()) {
      return;
    }

    await MySharedPreference.save(key: Constants.USER_FIRST_NAME, value: _txtNameController.text);
    await MySharedPreference.save(key: Constants.USER_LAST_NAME, value: _txtSurnameController.text);
    await MySharedPreference.save(key: Constants.USER_MOBILE, value: _txtMobileController.text);
    await MySharedPreference.save(key: Constants.USER_EMAIL, value: _txtEmailController.text);
    _loadUserProfile();
    Navigator.pop(context);
  }

  _buildCameraPopupDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("Choose"),
      backgroundColor: Colors.transparent,
      content: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        decoration: BoxDecoration(
          border: Border.all(color: Colors.orangeAccent),
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage("assets/images/home_background.jpeg"),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
          ),
        ),
        child: Column(
          children: [
            //Image.asset('assets/images/jra_splash2.png', width: MediaQuery.of(context).size.width * 0.4,),
            ListTile(
              onTap: () { Navigator.pop(context); _getFromCamera(ImageSource.camera); },
              leading: Icon(Icons.camera_alt_outlined, color: Constants.iconColor,),
              title: Text('Take a picture', style: TextStyle(color: Colors.orangeAccent),),
            ),
            Expanded(child: Divider()),
            ListTile(
              onTap: () { Navigator.pop(context); _getFromCamera(ImageSource.gallery); },
              leading: Icon(Icons.image, color: Constants.iconColor,),
              title: Text('Choose from Library', style: TextStyle(color: Colors.orangeAccent),),
            ),
            /**ElevatedButton(
                onPressed: () { Navigator.pop(context); },
                child: Text('CANCEL', style: TextStyle(color: Constants.btnTextColor),),
                style: ElevatedButton.styleFrom(
                primary: Constants.btnColor,
                minimumSize: Size(double.infinity, MediaQuery.of(context).size.height * 0.06), // double.infinity is the width and 30 is the height
                ),
                ),*/
          ],
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  /// Get from camera
  _getFromCamera(ImageSource source) async {
    var pickedFile = await ImagePicker().pickImage(
      source: source,
      maxWidth: 1800,
      maxHeight: 1800,
    );

    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);
      setState(() {
        profilePicture = pickedFile.path;
      });
      await MySharedPreference.save(key: Constants.PROFILE_PICTURE, value: pickedFile.path);
    }
  }
}
