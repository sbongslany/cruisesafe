import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/ui/views/support/support_screen1.dart';
import 'package:cruisesafe/ui/views/support/support_screen2.dart';
import 'package:cruisesafe/ui/widget/nav_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarouselWithIndicatorSupportScreen extends StatefulWidget {
  bool fromSupport;
  CarouselWithIndicatorSupportScreen({this.fromSupport = false});
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorSupportScreenState();
  }
}

class _CarouselWithIndicatorSupportScreenState extends State<CarouselWithIndicatorSupportScreen> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  List<Widget> myData = [
    SupportScreen2(),
    SupportScreen1(),
  ];

  @override
  void initState() {
    super.initState();
    if(widget.fromSupport) {
      setState(() {
        myData = [
          SupportScreen1(),
          SupportScreen2(),
        ];
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: Constants.backGroundDeco(),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          SizedBox(
            height: 10,
          ),
          //Text("Cruising Jo'burg roads with ease", style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold)),
          Flexible(
            child: CarouselSlider(
              items: myData,
              carouselController: _controller,
              options: CarouselOptions(
                  height: MediaQuery.of(context).size.height,
                  autoPlay: false,
                  enableInfiniteScroll: false,
                  //enlargeCenterPage: true,
                  //aspectRatio: 6.0,
                  viewportFraction: 1.0,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: myData.asMap().entries.map((entry) {
              return GestureDetector(
                onTap: () => _controller.animateToPage(entry.key),
                child: Container(
                  width: 12.0,
                  height: 12.0,
                  margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: (Theme.of(context).brightness == Brightness.dark
                              ? Colors.orangeAccent
                              : Colors.orangeAccent)
                          .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                ),
              );
            }).toList(),
          ),
        ]),
      ),
    );
  }
}
