import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/ui/views/home/FAQ/faq_screen.dart';
import 'package:cruisesafe/ui/widget/ExpansionList.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SupportScreen1 extends StatefulWidget {
  const SupportScreen1({Key? key}) : super(key: key);

  @override
  _SupportScreen1State createState() => _SupportScreen1State();
}

class _SupportScreen1State extends State<SupportScreen1> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: Constants.WIDGET_ALL_SIDES_MARGINS, right: Constants.WIDGET_ALL_SIDES_MARGINS),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              //Center(child: Icon(Icons.contact_phone_rounded, size: 60, color: Constants.iconColor,)),
              Center(child: Text('Contact Us', style: TextStyle(decoration: TextDecoration.underline, fontSize: 30, fontWeight: FontWeight.bold, color: Constants.lblColor2),)),
              SizedBox(height: 40,),
              Text('Customer Contact Center:', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Constants.lblColor2),),
              Row(
                children: [
                  Icon(Icons.mail,color: Constants.iconColor,),
                  SizedBox(width: 20,),
                  Text('hotline@jra.org.za', style: TextStyle(color: Constants.lblColor2),),
                ],
              ),
              SizedBox(height: 10,),
              Row(
                children: [
                  Icon(Icons.phone_rounded,color: Constants.iconColor,),
                  SizedBox(width: 20,),
                  Text('0800 002 587', style: TextStyle(color: Constants.lblColor2)),
                ],
              ),
              SizedBox(height: 20,),
              Divider(thickness: 1, color: Colors.white,),
              SizedBox(height: 20,),
              //SizedBox(height: 20,),
              Text('Please call Joburg connect to report any road related faults ', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Constants.lblColor2),),
              SizedBox(height: 10,),
              Row(
                children: [
                  Icon(Icons.phone_rounded,color: Constants.iconColor,),
                  SizedBox(width: 20,),
                  Text('Tel: +27(0) 11 298 5000', style: TextStyle(color: Constants.lblColor2)),
                ],
              ),
              SizedBox(height: 10,),
              Row(
                children: [
                  Icon(Icons.phone_rounded,color: Constants.iconColor,),
                  SizedBox(width: 20,),
                  Text('Fax: +27(0) 11 298 5178' , style: TextStyle(color: Constants.lblColor2)),
                ],
              ),
              SizedBox(height: 20,),
              Divider(thickness: 1, color: Constants.lblColor2,),
              SizedBox(height: 20,),
              Center(child: Text('Please take note of our frequently asked questions for further clarity', style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold, color: Constants.lblColor2),)),
              SizedBox(height: 60,),
              Center(
                child: Container(
                  height: 50,
                  width: 300,
                  //color: Constants.btnColor,
                  child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Constants.btnColor,
                      ),
                      onPressed: (){
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => FaqScreen()));
                      },

                      child: Row(
                       // mainAxisAlignment: MainAxisAlignment.,
                        children: [
                          Icon(Icons.question_answer_outlined, color: Constants.lblColor2),
                          SizedBox(width: 90,),
                          Text("FAQ'S")
                        ],
                      )
                  ),
                ),
              )
            ],
          ),
      ),
    );
  }
}
