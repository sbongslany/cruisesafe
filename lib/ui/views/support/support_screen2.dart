import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SupportScreen2 extends StatefulWidget {
  const SupportScreen2({Key? key}) : super(key: key);

  @override
  _SupportScreen2State createState() => _SupportScreen2State();
}

class _SupportScreen2State extends State<SupportScreen2> {

  void _downloadClaim() async {
    var url = "https://athandweserver.dedicated.co.za:52202/Public_Liability_Claim.pdf";
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false);
    } else {
      print('Could not launch $url');
    }
  }


  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(left: Constants.WIDGET_ALL_SIDES_MARGINS, right: Constants.WIDGET_ALL_SIDES_MARGINS),
        child: Column(
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            //Center(child: Icon(Icons.assignment_sharp, size: 60, color: Constants.iconColor,)),
            Center(child: Text('Claim For Vehicle Damage', textAlign: TextAlign.center, style: TextStyle(decoration: TextDecoration.underline, fontSize: 24, fontWeight: FontWeight.bold, color: Constants.lblColor2),)),
            SizedBox(height: 30,),
            Center(child: Text('Public Liability Claims (damage to vehicle, property or personal injury)', textAlign: TextAlign.center,style: TextStyle(color: Constants.lblColor2, fontSize: 14),)),
            SizedBox(height: 30,),
            Center(
              child: Text('We understand that a bust tyre or unexpected dent can set you back significantly. Even worse are accidents or '
                  'injuries. If this damage is linked to the road infrastructure that we maintain in '
                  'the City of Johannesburg you can file a claim for damages', textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Constants.lblColor2)),
            ),
            SizedBox(height: 30,),
            Center(
              child: Text('In order for your claim to considered please ,forward a complete '
                  'claim form with applicable supporting document as listed on the form to:'
              , textAlign: TextAlign.center, style: TextStyle(fontSize: 14, color: Constants.lblColor2),),
            ),
            SizedBox(height: 30,),
            ListTile(
              leading: Icon(Icons.mail,color: Constants.iconColor,),
              title: Text('claims@jra.org.za', style: TextStyle(fontSize: 16, color: Constants.lblColor2),),
            ),
            ListTile(
              leading: Icon(Icons.phone_rounded,color: Constants.iconColor,),
              title: Text('011 491-5734/011 298 5168', style: TextStyle(fontSize: 16, color: Constants.lblColor2),),
            ),
            ListTile(
              leading: Icon(Icons.contact_phone_rounded,color: Constants.iconColor,),
              title: Text('086 206 5294', style: TextStyle(fontSize: 16, color: Constants.lblColor2),),
            ),
            SizedBox(height: 30,),
            GestureDetector(
              onTap: () {
                _downloadClaim();
              },
              child: RichText(
                text: TextSpan(
                  children: [
                    TextSpan(text: 'Click ', style: TextStyle(fontSize: 16, color: Constants.lblColor2)),
                    TextSpan(text: 'here', style: TextStyle(fontSize: 16, color: Constants.btnColor)),
                    TextSpan(text: ' to download a copy of a Claims form', style: TextStyle(fontSize: 16, color: Constants.lblColor2)),
                  ]
                ),
              ),
            ),
            SizedBox(height: 35,),
            Text('Please note that receipts of your claim is done on a without prejudice basis. '
                'Each claim is assessed onn its own merits and acceptance of the claim does not imply '
                'automatic acceptance of validity claim, admission of liability or'
                ' commitment to the settlement of the claim.', style: TextStyle(fontSize: 14, fontStyle: FontStyle.italic, color: Constants.lblColor2),),
          ],
        ),
      ),
    );
  }
}
