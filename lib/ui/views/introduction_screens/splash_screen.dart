import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/ui/views/authentication/landing_screen.dart';
import 'package:cruisesafe/ui/views/home/home_screen.dart';
import 'package:cruisesafe/ui/views/introduction_screens/welcome_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:splash_screen_view/SplashScreenView.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> with SingleTickerProviderStateMixin{

  void startTimer() async{

    var userId = (await MySharedPreference.get(Constants.USER_ID) ?? "").toString();
    String email = (await MySharedPreference.get(Constants.USER_ID) ?? "").toString();
    String surname = (await MySharedPreference.get(Constants.USER_ID) ?? "").toString();
    String firstName = (await MySharedPreference.get(Constants.USER_ID) ?? "").toString();
    String profilePicture = (await MySharedPreference.get(Constants.PROFILE_PICTURE) ?? "").toString();



    //this will take 10 seconds, setting up location
    try {
      Position? position = await CruiseSafeLocation.reloadGeoLocationPosition();
    }catch(e) {
      print(e);
    }

    Future.delayed(Duration(seconds: 1), () {
      if(userId == "") {
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => LandingScreen()));
      }else{
        //int i = int.parse(userId);
        Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen(userId, email, profilePicture, surname, firstName)));
      }
    });
  }


  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: Constants.backGroundDecoLighter(img:"assets/images/splash_background.jpeg"),
            ),
            Positioned(left: 0, top: 0, child: Container(height: 200, width: MediaQuery.of(context).size.width, color: Constants.splash,)),
            Positioned(left: 20, top: 80, child: Text("#JRAInAction", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22))),
            Positioned(left: 20, top: 110, child: Text("#WeServeJoburg", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),)),
            Positioned(right: 15, top: 110, child: circle()),
          ],
        ),
    );
  }

  Widget circle() {
    return Stack(
      children: [
        Positioned(
            child: Container(
              height: 150,
              width: 150,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(width: 2, color: Colors.white),
                  color: Colors.grey[700]
              ),
            )
        ),
        Positioned(
          left: 25,
          top: 68,
          child: Text('JRA-FIXIT', textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20, color: Colors.white,)),
        ),
      ],
    );
  }
}
