import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/ui/views/authentication/landing_screen.dart';
import 'package:cruisesafe/ui/widget/social_button_widget.dart';
import 'package:cruisesafe/ui/widget/carousel.dart';
import 'package:cruisesafe/ui/widget/submit_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  // color: Colors.orange,
                  height: 150,
                  //width: MediaQuery.of(context).size.width,

                  //width: ,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image.asset('assets/images/jra_splash2.png'),
                    ],
                  ),
                ),
                Container(
                  height: 15,
                  width: MediaQuery.of(context).size.width,
                  color: Constants.getPrimaryColor(),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 300,
                  // color: Colors.red,
                  child: CarouselWithIndicator(),
                ),
                SizedBox(
                  height: 100,
                ),
                Container(
                  width: 250,
                  height: 50,
                  // color: Colors.orange,
                  child: SubmitButton(
                    label: 'Get Started', onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) => LandingScreen()));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
