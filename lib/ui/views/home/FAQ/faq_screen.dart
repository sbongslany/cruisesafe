import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class FaqScreen extends StatefulWidget {
  const FaqScreen({Key? key}) : super(key: key);

  @override
  _FaqScreenState createState() => _FaqScreenState();
}

class _FaqScreenState extends State<FaqScreen> {

  var questions = [];

  /**List<Entry> data =[
    Entry(
      'How do i log a service defect request',
      <Entry>[
        Entry(
            'You can report related faults here on the app \n\nOr call Joburg Connect '
                'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
        ),
      ],
    ),
  ];*/

  List<Entry> data =[];

  _getCategories() async {
    http.Response? response = await ReportService().getFAQ(context: context);
    if(response == null) {
      return;
    }

    var results = jsonDecode(response.body);
    setState(() {
      for(int i = 0; i < results.length; i++) {
        var entry = Entry(
          results[i]['faq']['faq_question'],
          <Entry>[
            Entry(
              results[i]['faq_answers'][0]['faq_answer'],
            ),
          ],
        );
        data.add(entry);
      }
      //questions = results;
    });
  }

  @override
  void initState() {
    super.initState();
    _getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: Constants.backGroundDeco(),
        child: Column(
          children: [
            SizedBox(height: 40,),
            ListTile(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
            Center(
              child: Text(
                " FAQ's ",
                style: TextStyle(fontSize: 20,decoration: TextDecoration.underline, fontWeight: FontWeight.bold, color: Constants.lblColor2),
              ),
            ),
            Expanded(
              child: ListView.separated(
                separatorBuilder: (context, index) => Divider(
                  color: Constants.lblColor2,
                ),
                itemBuilder: (BuildContext context, int index) => EntryItem(data[index]),
                itemCount: data.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;

}

/**final List<Entry> data =[
  Entry(
    'How do i log a service defect request',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),

  Entry(
    'How long will it take to fix an issue',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
  Entry(
    'How do i clain for public Liability \n'
        '(damage to vehicle, property or\n'
        'personal)? ',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
  Entry(
    'What information should i provide when \n'
        'reporting / submitting a public liability\n'
        'claim for damage to vehicle, property or personal inquiry',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
];*/

class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title, style: TextStyle(color: Constants.btnColor),));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title, style: TextStyle(fontWeight: FontWeight.bold, color: Constants.lblColor2), ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
