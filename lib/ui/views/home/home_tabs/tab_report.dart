import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/views/reports/open_report_comments.dart';
import 'package:cruisesafe/ui/views/reports/open_report_ticket.dart';
import 'package:cruisesafe/ui/views/reports/report_modal.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:cruisesafe/ui/widget/top_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;

class TabReportScreen extends StatefulWidget {
  const TabReportScreen({Key? key}) : super(key: key);
  @override
  _TabReportScreenState createState() => _TabReportScreenState();
}

class _TabReportScreenState extends State<TabReportScreen> {

  List reports = [];
  bool loadingStatus = true;

  void _loadReports() async {
    try {
      http.Response? response = await ReportService().getMyReports();
      //print(response?.body);
      if(response == null) {
        SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
        return;
      }
      setState(() {
        reports = jsonDecode(response.body);
      });

    }catch(e){
      print(e);
    }
    finally{
      setState(() {
        loadingStatus = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    this._loadReports();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reports'),
      ),
      body: SafeArea(
        child: Column(
          children: [
            //TopNav(),
            Expanded(
              child: Container(
                margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                child: Column(
                  children: [
                    /**Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Reports", style: TextStyle(fontSize: 20),),
                        IconButton(onPressed: (){  addReportBtn(); }, icon: Icon(Icons.add_circle_outline, color: Constants.iconColor, size: 40,))
                      ],
                    ),*/
                    SizedBox(height: 10,),
                    loadingStatus ? Center(child: Text('Loading reports...')) :
                    Expanded(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount:reports.length,
                          itemBuilder: (context, index){
                            //print(reports[index]['report_it_comment']);
                            return  reportCard(
                                reportType: reports[index]['report_itt']['reporting_category'],
                                time: reports[index]['report_itt']['reporting_timestamp'],
                                message: reports[index]['report_itt']['reporting_body'],
                                commentCount: '' + reports[index]['report_it_comment'].length.toString(),
                                status: reports[index]['report_itt']['reporting_app_status'],
                              ticketObject: reports[index]
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        onPressed: () {
          addReportBtn();
        },
        child: Icon(Icons.add, color: Colors.white,),
      ),
    );
  }

  void addReportBtn() {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ReportModal()));
  }

  Widget reportCard({reportType, time, message, commentCount, status, ticketObject}) {
    return GestureDetector(
      onTap: () {
        _buildPopupDialog(context, ticketObject);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 20),
        padding: const EdgeInsets.all(15.0),
        decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(5)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(FontAwesomeIcons.trafficLight, size: 18, color: Constants.getPrimaryColor(),),
                    Text(reportType),
                  ],
                ),
                Text(time),
              ],
            ),
            SizedBox(height: 10,),
            Text(message),
            SizedBox(height: 10,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Comments(' + commentCount + ')', style: TextStyle(fontSize: 12),),
                Text(status, style: TextStyle(fontSize: 12)),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _buildPopupDialog(BuildContext context, var ticketObject) {
    // set up the button
    /**Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );*/

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("Choose"),
      content: Container(
        height: MediaQuery.of(context).size.height * 0.5,
        child: Column(
          children: [
            Image.asset('assets/images/jra_splash2.png', width: MediaQuery.of(context).size.width * 0.4,),
            ListTile(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => OpenReportComments(ticketObject)));
                },
              leading: Icon(Icons.comment, color: Constants.iconColor,),
              title: Text('View Comments'),
            ),
            Expanded(child: Divider()),
            ListTile(
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, MaterialPageRoute(builder: (context) => OpenReportTicket()));
                },
              leading: Icon(Icons.edit, color: Constants.iconColor,),
              title: Text('Open Service Request'),
            ),
            Expanded(child: Divider()),
            ListTile(
              onTap: () { Navigator.pop(context); },
              leading: Icon(Icons.delete, color: Constants.iconColor,),
              title: Text('Delete Service Request'),
            ),
            ElevatedButton(
              onPressed: () { Navigator.pop(context); },
              child: Text('CANCEL', style: TextStyle(color: Constants.btnTextColor),),
              style: ElevatedButton.styleFrom(
                primary: Constants.btnColor,
                minimumSize: Size(double.infinity, MediaQuery.of(context).size.height * 0.05), // double.infinity is the width and 30 is the height
              ),
            ),
          ],
        ),
      ),
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

}
