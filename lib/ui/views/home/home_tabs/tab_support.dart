import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/ui/widget/carousel.dart';
import 'package:cruisesafe/ui/widget/submit_button.dart';
import 'package:cruisesafe/ui/views/support/support_carousel.dart';
import 'package:cruisesafe/ui/widget/top_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabSupportScreen extends StatefulWidget {
  bool fromSupport;
  TabSupportScreen({this.fromSupport = false});
  @override
  _TabSupportScreenState createState() => _TabSupportScreenState();
}

class _TabSupportScreenState extends State<TabSupportScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /**appBar: AppBar(
        title: Text('Support'),
      ),*/
      body: Container(
          decoration: Constants.backGroundDeco(),
          child: Column(
            children: [
              SizedBox(height: 40,),
              ListTile(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
              ),
              Expanded(child: CarouselWithIndicatorSupportScreen(fromSupport: widget.fromSupport,)),
            ],
          )
      ),
    );
  }
}
