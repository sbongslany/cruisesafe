import 'dart:async';
import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/views/reports/report_modal.dart';
import 'package:cruisesafe/ui/views/reports/reported_issues_google_map_screen.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:geocoding/geocoding.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:location/location.dart';
import 'package:http/http.dart' as http;


class TabHomeScreen extends StatefulWidget {
  //const TabHomeScreen({Key? key}) : super(key: key);
  //String name;
  //TabHomeScreen(this.name);
  @override
  _TabHomeScreenState createState() => _TabHomeScreenState();
}

class _TabHomeScreenState extends State<TabHomeScreen> {
  /**Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 14.4746,
  );

  LatLng _initialcameraposition = LatLng(20.5937, 78.9629);*/


  final _gKey = new GlobalKey<ScaffoldState>();
  String fullName = "";

  LatLng _initialcameraposition = LatLng(-30.745048104080578, 24.91135586053133);
  LatLng _initialcameraposition2 = LatLng(-30.745048104080578, 24.91135586053133);

  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  MarkerId? selectedMarker;
  int _markerIdCounter = 1;
  LatLng? markerPosition;
  double latitude = -30.745048104080578;
  double longitude = 24.91135586053133;

  final double _CAMERA_CENTRE = 15;

  bool displayMap = false;
  List localReports = [];

  late GoogleMapController _controller;
  late GoogleMapController _controller2;
  Location _location = Location();
  Location _location2 = Location();

  void _loadProfile() async {
    /**var ff =  await MySharedPreference.get(Constants.USER_FULL_NAME);
    setState(() {
      fullName = ff;
    });*/
  }

  void _loadUserProfile() async {
    String fN = await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "";
    /**String sN = await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "";
    String pM = await MySharedPreference.get(Constants.USER_MOBILE) ?? "";
    String eM = await MySharedPreference.get(Constants.USER_EMAIL) ?? "";
    String lT = await MySharedPreference.get(Constants.LOGIN_TYPE) ?? "";*/

    setState(() {
      fullName = fN;
    });
  }

  void _loadReportsAroundYou() async {
    try {
      http.Response? response = await ReportService().getReportsAroundYou(context: context);
      //print(response?.body);
      if(response == null) {
        //SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Sorry, could not load road issues around you.");
        return;
      }

      //print(response.body);
      var json = jsonDecode(response.body);
      print(json);
      setState(() {
        localReports = jsonDecode(response.body);
      });

      for(int k = 0; k < localReports.length; k++) {
        _addMarker( double.parse(localReports[k]['ticket_latitude']), double.parse(localReports[k]['ticket_longitude']));
      }
      //_addMarker(lat, longi)

    }catch(e){
      print(e);
    }
    finally{

    }
  }

  _onMapCreated(GoogleMapController _cntlr) async {
    print('_onMapCreated....');
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    setState(() {
      _initialcameraposition = LatLng(position.latitude, position.longitude);
      latitude = position.latitude;
      longitude = position.longitude;
      _controller = _cntlr;
      _location.onLocationChanged.listen((l) {
        _controller.animateCamera(
          CameraUpdate.newCameraPosition(
            //CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
            CameraPosition(
                target: LatLng(l.latitude ?? position.latitude, l.longitude ?? position.longitude),
                zoom: _CAMERA_CENTRE
            ),
          ),
        );
      });
    });
  }


  _onMapCreatedDefault(GoogleMapController _cntlr) async {
    print('_onMapCreated....');
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    setState(() {
      _initialcameraposition2 = LatLng(position.latitude, position.longitude);
      _controller2 = _cntlr;
      _location2.onLocationChanged.listen((l) {
        _controller2.animateCamera(
          CameraUpdate.newCameraPosition(
            //CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
            CameraPosition(
                target: LatLng(l.latitude ?? position.latitude, l.longitude ?? position.longitude),
                zoom: _CAMERA_CENTRE
            ),
          ),
        );
      });
    });
  }

  void _onMarkerTapped(MarkerId markerId) {
    final Marker? tappedMarker = markers[markerId];
    if (tappedMarker != null) {
      setState(() {
        final MarkerId? previousMarkerId = selectedMarker;
        if (previousMarkerId != null && markers.containsKey(previousMarkerId)) {
          final Marker resetOld = markers[previousMarkerId]!
              .copyWith(iconParam: BitmapDescriptor.defaultMarker);
          markers[previousMarkerId] = resetOld;
        }
        selectedMarker = markerId;
        final Marker newMarker = tappedMarker.copyWith(
          iconParam: BitmapDescriptor.defaultMarkerWithHue(
            BitmapDescriptor.hueGreen,
          ),
        );
        markers[markerId] = newMarker;

        markerPosition = null;
      });
    }
  }

  void _onMarkerDrag(MarkerId markerId, LatLng newPosition) async {
    setState(() {
      this.markerPosition = newPosition;
    });
  }

  void _onMarkerDragEnd(MarkerId markerId, LatLng newPosition) async {
    final Marker? tappedMarker = markers[markerId];
    if (tappedMarker != null) {
      setState(() {
        this.markerPosition = null;
      });
      await showDialog<void>(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
                actions: <Widget>[
                  TextButton(
                    child: const Text('OK'),
                    onPressed: () => Navigator.of(context).pop(),
                  )
                ],
                content: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 66),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Text('Old position: ${tappedMarker.position}'),
                        Text('New position: $newPosition'),
                      ],
                    )));
          });
    }
  }

  void _add() {
    final int markerCount = markers.length;

    if (markerCount == 12) {
      return;
    }

    final String markerIdVal = 'marker_id_$_markerIdCounter';
    _markerIdCounter++;
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        20.0,
        20.0,
      ),
      infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
      onTap: () => _onMarkerTapped(markerId),
      onDragEnd: (LatLng position) => _onMarkerDragEnd(markerId, position),
      onDrag: (LatLng position) => _onMarkerDrag(markerId, position),
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  void _addMarker(lat, longi) {
    final int markerCount = markers.length;

    if (markerCount == 5) {
      return;
    }

    final String markerIdVal = 'marker_id_$_markerIdCounter';
    _markerIdCounter++;
    final MarkerId markerId = MarkerId(markerIdVal);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        lat,
        longi,
      ),
      infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
      onTap: () => _onMarkerTapped(markerId),
      onDragEnd: (LatLng position) => _onMarkerDragEnd(markerId, position),
      onDrag: (LatLng position) => _onMarkerDrag(markerId, position),
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  void _remove(MarkerId markerId) {
    setState(() {
      if (markers.containsKey(markerId)) {
        markers.remove(markerId);
      }
    });
  }


  void saveReport({typeOfReport}) async {
    String userId = await MySharedPreference.get(Constants.USER_ID) ?? "";
    String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);
    print(placeMark);
    //'${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    http.Response? response = await ReportService().saveReport(
        context: context,
      longitude: position.longitude,
      latitude: position.latitude,
      comment: typeOfReport + " - User Quick Reporting",
      reportingType: typeOfReport,
      personReporting: personReporting,
      userId: userId,
      fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
      locationType: 'My Location',
      country: placeMark.country,
      metropolitan: placeMark.subAdministrativeArea,
      postalCode: placeMark.postalCode,
      province: placeMark.administrativeArea,
      street: placeMark.street,
      suburb: placeMark.subLocality,
      town: placeMark.locality,
      personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString()
    );

    /**if(response == null) {
      SnackBarWidget.showToast(context: context, message: "Error occurred!");
      return;
    }

    var results = jsonDecode(response.body);
    print(response.body);**/

    SnackBarWidget.showToast(context: context, message: 'Submitted successfully');
    /**if(response.statusCode == 201) {
      SnackBarWidget.showToast(context: context, message: results['message']);
    }else if(response.statusCode == 400) {
      SnackBarWidget.showToast(context: context, message: results['message']);
    }else{
      SnackBarWidget.showToast(context: context, message: "Error occurred!");
    }*/
  }

  _loadCurrentLocation() async {
    Position? position = await CruiseSafeLocation.reloadGeoLocationPosition();

    //print('Current location: ' + await CruiseSafeLocation.getAddressFromPosition(position));
    setState(() {
      if(!displayMap) {
        displayMap = true;
      }
      _initialcameraposition = LatLng(position!.latitude, position.longitude);
      latitude = position.latitude;
      longitude = position.longitude;
      print('Current location Loaded successfully.');
    });

  }

  @override
  void initState() {
    super.initState();
    _loadCurrentLocation();
    _loadReportsAroundYou();
    _loadUserProfile();
    print('Home loaded...');
    Future.delayed(Duration.zero, () {
      //_loadReportsAroundYou();
      //_loadCurrentLocation();
      //_onMapCreated(GoogleMapController _cntlr);
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      key: _gKey,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/home_background.jpeg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //TopNav(),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  margin: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome, " + fullName,
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: Constants.SPACE_BETWEEN_TXT,
                      ),
                      Text(
                        "Your ultimate driving foresight",
                        style: TextStyle(fontSize: 16),
                      ),
                      SizedBox(
                        height: Constants.SPACE_BETWEEN_TXT,
                      ),
                      GestureDetector(
                        onTap: () {
                          /**Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => QuickReport()),
                          );*/
                          //displayBottomSheet();
                          _buildPopForReportingTypeUpDialog(context);
                        },
                        child: Card(
                          color: Constants.cardColor,
                          elevation: 15,
                          child: ListTile(
                            leading: Icon(
                              Icons.location_on_outlined,
                              color: Constants.iconColor,
                              size: 40,
                            ),
                            title: Text(
                              'Quick Report',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Constants.cardLabelColor),
                            ),
                            subtitle: Text(
                              '\nTwo button click to report issues which captures the incident and location',
                              style: TextStyle(color: Constants.cardLabelColor, fontSize: 12),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Constants.SPACE_BETWEEN_TXT,
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ReportModal()));
                        },
                        child: Card(
                          color: Constants.cardColorW,
                          elevation: 15,
                          child: ListTile(
                            leading: Icon(
                              Icons.warning,
                              color: Constants.iconColor,
                              size: 40,
                            ),
                            title: Text(
                              'Detailed Reporting',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.bold,
                                  color: Constants.cardLabelColorW),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: Constants.SPACE_BETWEEN_TXT,
                      ),
                      Container(
                        width: double.infinity,
                        child: Card(
                          color: Constants.cardColorW,
                          elevation: 15,
                          child: Container(
                            padding: EdgeInsets.all(10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text('Reported issues around you'),
                                    GestureDetector(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                ReportedIssuesGoogleMapScreen()));
                                      },
                                      child: Container( color: Colors.black, padding: EdgeInsets.all(5),child: Text('view', style: TextStyle(color: Colors.white),)),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5,
                                ),
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.3,
                                  child: !displayMap ? Text('') :  GoogleMap(
                                    initialCameraPosition: CameraPosition(
                                      zoom: _CAMERA_CENTRE,
                                      target: LatLng(latitude, longitude),
                                    ),
                                    mapType: MapType.hybrid,
                                    onMapCreated: _onMapCreated,
                                    myLocationEnabled: true,
                                    markers: Set<Marker>.of(markers.values),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildPopForReportingTypeUpDialog(BuildContext context) {
    // set up the button
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("Choose"),
      scrollable: true,
      content: Container(
          decoration: new BoxDecoration(
            //color: Colors.grey.shade300,
          ),
          height: MediaQuery.of(context).size.height * 0.75,
          //width: double.infinity,
          width: double.infinity,
          //color: Colors.grey.shade200,
          //alignment: Alignment.center,
          child: Container(
            child: Column(
              children: [
                Image(image: AssetImage("assets/images/jra_splash2.png"), width: MediaQuery.of(context).size.width * 0.3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      icon: 'assets/images/pothole.png',
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      label: 'Pothole',
                      onPressed: (){
                        buttonOptionPressed(option: 'Pothole');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      icon: 'assets/images/traffic_lights.png',
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      label: 'Traffic Light',
                      onPressed: (){
                        buttonOptionPressed(option: 'Traffic Light');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/bad_road.png',
                      label: 'Bad Road',
                      onPressed: (){
                        buttonOptionPressed(option: 'Bad Road');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/drainage.png',
                      label: 'Water Drain',
                      onPressed: (){
                        buttonOptionPressed(option: 'Water Drain');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/manhole.png',
                      label: 'Manhole Cover',
                      onPressed: (){
                        buttonOptionPressed(option: 'Manhole Cover');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/crack.png',
                      label: 'Road Cracks',
                      onPressed: () {
                        buttonOptionPressed(option: 'Road Cracks');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width * 0.9,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      primary: Colors.grey, // Background color
                    ),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text("Cancel"),
                  ),
                ),
              ],
            ),
          )
      ),
    );

    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }



  //https://api.flutter.dev/flutter/material/showModalBottomSheet.html
  void displayBottomSheet()
  {
    /**_gKey.currentState!.showBottomSheet((context)
    {
      return Container(
          decoration: new BoxDecoration(
              color: Colors.grey.shade300,
              borderRadius: new BorderRadius.only(
                topLeft: const Radius.circular(40.0),
                topRight: const Radius.circular(40.0),
              )
          ),
        height: MediaQuery.of(context).size.height * 0.8,
        //width: double.infinity,
          width: double.infinity,
        //color: Colors.grey.shade200,
        //alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.all(20),
          child: Column(
            children: [
              //Image(image: AssetImage("assets/images/jra_splash2.png"), width: MediaQuery.of(context).size.width * 0.3,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/pothole.png'),
                    label: 'Pothole',
                    onPressed: (){
                      buttonOptionPressed(option: 'Pothole');
                    },
                  ),
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/traffic_lights.png'),
                    label: 'Traffic Light',
                    onPressed: (){
                      buttonOptionPressed(option: 'Traffic Light');
                    },
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/bad_road.png'),
                    label: 'Bad Road',
                    onPressed: (){
                      buttonOptionPressed(option: 'Bad Road');
                    },
                  ),
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/drainage.png'),
                    label: 'Water Drain',
                    onPressed: (){
                      buttonOptionPressed(option: 'Water Drain');
                    },
                  ),
                ],
              ),
              SizedBox(height: 20,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/manhole.png'),
                    label: 'Manhole Cover',
                    onPressed: (){
                      buttonOptionPressed(option: 'Manhole Cover');
                    },
                  ),
                  RoundButton(
                    buttonSize: MediaQuery.of(context).size.height * 0.14,
                    icon: Image.asset('assets/images/crack.png'),
                    label: 'Road Cracks',
                    onPressed: (){
                      buttonOptionPressed(option: 'Road Cracks');
                    },
                  ),
                ],
              ),
              SizedBox(height: 20,),
              ButtonTheme(
                minWidth: MediaQuery.of(context).size.width * 0.9,
                child: RaisedButton(
                  color: Colors.grey,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Cancel"),
                ),
              ),
            ],
          ),
        )
      );
    }
    );*/
  }

  Widget loadInitMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: _initialcameraposition2, zoom: _CAMERA_CENTRE),
      mapType: MapType.normal,
      onMapCreated: _onMapCreatedDefault,
      myLocationEnabled: true,
    );
  }

  Widget loadMapWithCo(double lat, double longi) {
    print('Loading loadMapWithCo....');
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: LatLng(lat, longi), zoom: _CAMERA_CENTRE),
      mapType: MapType.normal,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
    );
  }

  void buttonOptionPressed({option}) {
    print(option ?? "No Option Selected");
    saveReport(typeOfReport: option ?? "No Option Selected");
    Navigator.of(context).pop();
  }
}
