import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/ui/widget/top_nav.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabProfileScreen extends StatefulWidget {
  const TabProfileScreen({Key? key}) : super(key: key);

  @override
  _TabProfileScreenState createState() => _TabProfileScreenState();
}

class _TabProfileScreenState extends State<TabProfileScreen> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //TopNav(),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Reports", style: TextStyle(fontSize: 20),),
                        IconButton(onPressed: (){ }, icon: Icon(Icons.edit, color: Constants.iconColor, size: 40,))
                      ],
                    ),
                    SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Last Name'),
                      subtitle: Text('Mkhize'),
                    ),
                    Divider(),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('First Name'),
                      subtitle: Text('Nhlanhla'),
                    ),
                    Divider(),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Phone Number'),
                      subtitle: Text('0734295463'),
                    ),
                    Divider(),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Email Address'),
                      subtitle: Text('mbmkhize22@gmail.com'),
                    ),
                    Divider(),
                    ListTile(
                      leading: Icon(Icons.person),
                      title: Text('Logged in with'),
                      subtitle: Text('Account'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
