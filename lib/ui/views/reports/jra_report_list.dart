import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/helpers.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/views/reports/report_modal.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'detailed_reporting_categories.dart';

class JraReportList extends StatefulWidget {
  bool detailedReporting = false;
  bool fromHome;
  JraReportList({required this.detailedReporting, this.fromHome = false});
  @override
  _JraReportListState createState() => _JraReportListState();
}

class _JraReportListState extends State<JraReportList> {

  List myReports = [];

  void _loadMyReports() async {
    try {
      http.Response? response = await ReportService().getMyReports(context: context);
      //print(response?.body);
      if(response == null) {
        //SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Sorry, could not load road issues around you.");
        return;
      }

      //print(response.body);
      var json = jsonDecode(response.body);
      setState(() {
        myReports = json;
      });
      print(json);

    }catch(e){
      print(e);
    }
    finally{

    }
  }


  @override
  void initState() {
    super.initState();
    print(widget.fromHome);
    print(widget.detailedReporting);
    Future.delayed(Duration.zero, () async {
      if(widget.detailedReporting != null && widget.detailedReporting) {
        await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ReportModal(fromHomeScreen: widget.fromHome,)));
      }
      _loadMyReports();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: Constants.backGroundDeco(),
        child: Column(
          children: [
            SizedBox(height: 40,),
            ListTile(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                onPressed: () {
                    Navigator.pop(context);
                },
              ),
              trailing: IconButton(
                icon: Icon(Icons.add_sharp, color: Colors.white,),
                onPressed: () async {
                  //Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => DetailedReportingCategories()));
                  await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => ReportModal(fromListScreen: true,)));
                  _loadMyReports();
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Column(
                children: [
                  Center(
                    child: Container(
                        padding: EdgeInsets.only(bottom: 8),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide( //                    <--- top side
                              color: Colors.white,
                              width: 1.0,
                            ),
                          ),
                        ),
                        child: RichText(
                          text: TextSpan(
                              children: [
                                TextSpan(text: 'MY  ', style: TextStyle(fontSize: 18, color: Constants.lblColor2, fontWeight: FontWeight.bold)),
                                TextSpan(text: 'REPORT', style: TextStyle(fontSize: 18, color: Constants.btnColor, fontWeight: FontWeight.bold)),
                              ]
                          ),
                        )
                    ),
                  ),
                  Center(
                    child: Text(
                      " STATUS",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white, letterSpacing: 5),
                    ),
                  ),
                  SizedBox(height: 10,),
                  
                  Container(
                    height: MediaQuery.of(context).size.height * 0.7,
                    //color: Colors.red,
                    child: ListView.builder(
                        shrinkWrap: true,
                        itemCount: myReports.length,
                        itemBuilder: (BuildContext context,int index){
                          return card(
                            data: myReports[index]
                          );
                        }
                    ),
                  ),
                ],
              )
            ),
          ],
        ),
      ),
    );
  }

  Widget card({data}) {
    String street = data['report_itt']['reporting_incident_location_street'].toUpperCase() +"," ?? "";
    String suburb = data['report_itt']['reporting_incident_location_suburb'].toUpperCase() + "," ?? "";
    String subCat = data['report_itt']['reporting_sub_category'].toUpperCase() + "" ?? "";
    String address = suburb + " " + street + " " + subCat;
    return GestureDetector(
      onTap: () {
        print(data);
      },
      child: Container(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(5),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white24),
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Center(child: Text(address, style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.bold), textAlign: TextAlign.center,))
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("DATE OF REPORT", style: TextStyle(color: Colors.white, fontSize: 14), textAlign: TextAlign.center,),
                Text(Helpers.checkDate(data['report_itt']['reporting_timestamp']) ?? "", style: TextStyle(color: Colors.white, fontSize: 12, ), textAlign: TextAlign.center,)
              ],
            ),
            SizedBox(height: 15,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text("STATUS", style: TextStyle(color: Colors.white, fontSize: 14), textAlign: TextAlign.center,),
                Text(data['report_itt']['reporting_status'].toUpperCase() ?? "", style: TextStyle(color: Colors.yellow, fontSize: 12, fontWeight: FontWeight.bold), textAlign: TextAlign.center,)
              ],
            ),
            SizedBox(height: 35,),
            Divider(
              height: 1,
              color: Colors.white,
            ),
            SizedBox(height: 15,),
          ],
        ),
      ),
    );
  }

  void buttonOptionPressed({option}) {
    print(option ?? "No Option Selected");
    saveReport(typeOfReport: option ?? "No Option Selected");
  }

  void saveReport({typeOfReport}) async {
    String userId = await MySharedPreference.get(Constants.USER_ID) ?? "";
    String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);
    print(placeMark);
    //'${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    http.Response? response = await ReportService().saveReport(
        context: context,
        longitude: position.longitude,
        latitude: position.latitude,
        comment: typeOfReport + " - User Quick Reporting",
        reportingType: typeOfReport,
        personReporting: personReporting,
        userId: userId,
        fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
        locationType: 'My Location',
        country: placeMark.country,
        metropolitan: placeMark.subAdministrativeArea,
        postalCode: placeMark.postalCode,
        province: placeMark.administrativeArea,
        street: placeMark.street,
        suburb: placeMark.subLocality,
        town: placeMark.locality,
        personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString()
    );

    print(response);

    if(response == null) {
        SnackBarWidget.showToast(context: context, message: "Error occurred!");
        return;
    }

    var results = jsonDecode(response.body);
    print(response.body);

    SnackBarWidget.showToast(context: context, message: 'Submitted successfully');
    Navigator.pop(context);
    /**if(response.statusCode == 201) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else if(response.statusCode == 400) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else{
        SnackBarWidget.showToast(context: context, message: "Error occurred!");
        }*/
  }
}
