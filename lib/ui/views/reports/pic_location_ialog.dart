import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class PicLocationDialog extends StatefulWidget {
  const PicLocationDialog({Key? key}) : super(key: key);

  @override
  _PicLocationDialogState createState() => _PicLocationDialogState();
}

class _PicLocationDialogState extends State<PicLocationDialog> {

  late GoogleMapController _controller;
  Location _location = Location();
  LatLng _initialcameraposition = LatLng(-30.745048104080578, 24.91135586053133);
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  String addressLocation = '';
  bool currentMapLoaded = false;

  Map<String, dynamic> res = <String, dynamic>{};

  void _onMapCreated(GoogleMapController _cntlr) {
    _controller = _cntlr;
    _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          //CameraPosition(target: LatLng(l.latitude, l.longitude),zoom: 15),
          CameraPosition(
              target: LatLng(l.latitude ?? _initialcameraposition.latitude,
                  l.longitude ?? _initialcameraposition.longitude),
              zoom: 80),
        ),
      );
    });
  }

  _updateLocation() async {
    Position position = await CruiseSafeLocation.getGeoLocationPosition();

    double lat = position.latitude;
    double longi = position.longitude;
    setState(() {
      _initialcameraposition = LatLng(lat, longi);
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, longi),zoom: 17),
        ),
      );
      if(!currentMapLoaded) {
        currentMapLoaded = true;
      }
    });
  }


  @override
  void initState() {
    super.initState();
    _updateLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Pick Location'),),
      body: Stack(
        children: [
          Positioned(
            child: !currentMapLoaded ? loadInitMap() : GoogleMap(
              initialCameraPosition: CameraPosition(
                  zoom: 11,
                  target: _initialcameraposition),
              mapType: MapType.hybrid,
              onMapCreated: _onMapCreated,
              myLocationEnabled: true,
              onTap: _mapTapped,
              zoomControlsEnabled: true,
              myLocationButtonEnabled: true,
              markers: Set<Marker>.of(markers.values),
            ),
          ),
          Positioned(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height * 0.065,
                color: Colors.black,
                child: Text(addressLocation, style: TextStyle(color: Colors.white),),
              ),
          ),
          Positioned(
            bottom: 40,
            child: Container(
              child: SizedBox(
                width: MediaQuery.of(context).size.width * 0.7,
                child: TextButton(
                  onPressed: () {
                    Navigator.pop(context, res);
                  },
                  style: TextButton.styleFrom(
                    backgroundColor: Constants.getPrimaryColor(),
                    padding: EdgeInsets.all(10.0),
                  ),
                  child: Text(
                    'Pick selected location',
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ],
      )
    );
  }

  _mapTapped(LatLng location) async{
    print(location);
    _add(location.latitude, location.longitude);
    String ff = await CruiseSafeLocation.getAddressFromLatLng(location.latitude, location.longitude);
    setState(() {
      addressLocation = ff;
      res['addressLocation'] = ff;
      res['latitude'] = location.latitude;
      res['longitude'] = location.longitude;
    });
    print(await CruiseSafeLocation.getAddressFromLatLng(location.latitude, location.longitude));
// The result will be the location you've been selected
// something like this LatLng(12.12323,34.12312)
// you can do whatever you do with it

  }

  void _add(double lat, double lng) {
    //final int markerCount = markers.length;

    /**if (markerCount == 12) {
      return;
    }*/

   // final String markerIdVal = 'marker_id_$markerCount';
    final String markerIdVal = 'marker_id_1';
    //_markerIdCounter++;
    final MarkerId markerId = MarkerId(markerIdVal);
    _remove(markerId);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        lat,
        lng,
      ),
      infoWindow: InfoWindow(title: markerIdVal, snippet: '*'),
      //onTap: () => _onMarkerTapped(markerId),
      //onDragEnd: (LatLng position) => _onMarkerDragEnd(markerId, position),
      //onDrag: (LatLng position) => _onMarkerDrag(markerId, position),
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  void _remove(MarkerId markerId) {
    setState(() {
      if (markers.containsKey(markerId)) {
        markers.remove(markerId);
      }
    });
  }

  Widget loadInitMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: _initialcameraposition, zoom: 5),
      mapType: MapType.normal,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
    );
  }

  Widget loadCurrentMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: _initialcameraposition, zoom: 14),
      mapType: MapType.hybrid,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      markers: Set<Marker>.of(markers.values),
    );
  }
}
