import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class QuickReporting extends StatefulWidget {
  const QuickReporting({Key? key}) : super(key: key);

  @override
  _QuickReportingState createState() => _QuickReportingState();
}

class _QuickReportingState extends State<QuickReporting> {
  var _category_id = 0, _category_sub_id = 0;
  String categoryName = "", categorySubName = "";
  List categories = [];

  _getCategories() async {
    http.Response? response = await ReportService().getCategories(context: context);
    if(response == null) {
      return;
    }

    var results = jsonDecode(response.body);
    List subCats = [];
    for(int i = 0; i< results.length; i++) {
      for(int k = 0; k < results[i]['category_subs'].length; k++) {
        if(results[i]['category_subs'][k]['category_sub_type'] == "quick reporting") {
          var dd = {
            "category_id": results[i]['category']['category_id'],
            "category_name": results[i]['category']['category_name'],
            "category_sub_id": results[i]['category_subs'][k]['category_sub_id'],
            "category_sub_name": results[i]['category_subs'][k]['category_sub_name']
          };
          subCats.add(dd);
        }
      }
    }
    setState(() {
      categories = subCats;
    });

    print(categories);
  }

  @override
  void initState() {
    super.initState();
    _getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage("assets/images/home_background.jpeg"),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
          ),
        ),
        child: Column(
          children: [
            SizedBox(height: 50,),
            ListTile(
              leading: IconButton(
                icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                onPressed: () {
                    Navigator.pop(context);
                },
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 15, right: 15),
              child: Column(
                children: [
                  Center(
                    child: Text(
                      "QUICK REPORTING",
                      style: TextStyle(fontSize: 20,decoration: TextDecoration.underline, fontWeight: FontWeight.bold, color: Colors.white),
                    ),
                  ),
                  //SizedBox(height: 10,),
                  ListView(
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      Container(
                        //color: Colors.red,
                        height: MediaQuery.of(context).size.height * 0.7,
                        child: GridView.count(
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          children: List.generate(categories.length, (index) {
                            return RoundButton(
                              buttonSize: MediaQuery.of(context).size.height * 0.13,
                              icon: 'assets/images/pothole.png',
                              iconWidth: MediaQuery.of(context).size.width * 0.15,
                              label: categories[index]['category_sub_name'],
                              onPressed: (){
                                //buttonOptionPressed(option: categories[index]['category']['category_name']);
                                print(categories[index]);
                                setState(() {
                                  /**subCategorySelected = true;
                                  category = categories[index]['category']['category_name'];
                                  reporting_category_id = categories[index]['category']['category_id'];
a
                                  subCategories = categories[index]['category_subs'];*/
                                  _category_id = categories[index]['category_id'];
                                  _category_sub_id = categories[index]['category_sub_id'];
                                  categoryName = categories[index]['category_name'];
                                  categorySubName = categories[index]['category_sub_name'];
                                  buttonOptionPressed(option: categoryName);
                                });
                              },
                            );
                          }),
                        ),
                      ),
                    ],
                  ),
                  /**Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        icon: 'assets/images/pothole.png',
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        label: 'Pothole',
                        onPressed: (){
                          buttonOptionPressed(option: 'Pothole');
                        },
                      ),
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        icon: 'assets/images/pothole.png',
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        label: 'Reinstatement/Trench across the road',
                        onPressed: (){
                          buttonOptionPressed(option: 'Reinstatement/Trench across the road');
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        icon: 'assets/images/pothole.png',
                        label: 'Street name on kerb',
                        onPressed: (){
                          buttonOptionPressed(option: 'Street name on kerb');
                        },
                      ),
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        icon: 'assets/images/pothole.png',
                        label: 'Guardrail',
                        onPressed: (){
                          buttonOptionPressed(option: 'Guardrail');
                        },
                      ),
                    ],
                  ),
                  SizedBox(height: 20,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        icon: 'assets/images/pothole.png',
                        label: 'Traffic light flashing red',
                        onPressed: (){
                          buttonOptionPressed(option: 'Traffic light flashing red');
                        },
                      ),
                      RoundButton(
                        buttonSize: MediaQuery.of(context).size.height * 0.13,
                        iconWidth: MediaQuery.of(context).size.width * 0.15,
                        icon: 'assets/images/pothole.png',
                        label: 'Blocked stormwater',
                        onPressed: () {
                          buttonOptionPressed(option: 'Road Cracks');
                        },
                      ),
                    ],
                  ),*/
                ],
              )
            ),
          ],
        ),
      ),
    );
  }

  void buttonOptionPressed({option}) {
    print(option ?? "No Option Selected");
    saveReport(typeOfReport: option ?? "No Option Selected");
  }

  void saveReport({typeOfReport}) async {
    String userId = await MySharedPreference.get(Constants.USER_ID) ?? "";
    String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);
    print(placeMark);
    //'${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    http.Response? response = await ReportService().saveReport(
        context: context,
        longitude: position.longitude,
        latitude: position.latitude,
        comment: categoryName + " - " + categorySubName +" (User Quick Reporting)",
        reportingType: typeOfReport,
        personReporting: personReporting,
        userId: userId,
        fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
        locationType: 'My Location',
        country: placeMark.country,
        metropolitan: placeMark.subAdministrativeArea,
        postalCode: placeMark.postalCode,
        province: placeMark.administrativeArea,
        street: placeMark.street,
        suburb: placeMark.subLocality,
        town: placeMark.locality,
        personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString(),
        reporting_category_id: _category_id,
        reporting_category_sub_id: _category_sub_id,
        category: categoryName,
        subCategory: categorySubName,
        reporting_title: "User Quick Reporting",

    );



    if(response == null) {
        SnackBarWidget.showToast(context: context, message: "Network Error occurred!");
        return;
    }

    var results = jsonDecode(response.body);
    print(response.statusCode);
    if(response.statusCode == 409) {
      print(response.body);
      SnackBarWidget.showSnackBar(context: context, color: Colors.red, message: results['value1']);
      return;
    }

    if(response.statusCode == 400) {
      print(response.body);
      SnackBarWidget.showSnackBar(context: context, color: Colors.red, message: results['value']);
      return;
    }

    if(response.statusCode == 201) {
      print(response.body);
      SnackBarWidget.showToast(context: context, message: 'Submitted successfully');
      Navigator.pop(context);
    }

    if(response.statusCode == 500) {
      print(response.body);
      SnackBarWidget.showToast(context: context, message: 'Submitted successfully');
      Navigator.pop(context);
    }



    /**if(response.statusCode == 201) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else if(response.statusCode == 400) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else{
        SnackBarWidget.showToast(context: context, message: "Error occurred!");
        }*/
  }
}
