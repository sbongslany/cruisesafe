import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

class DetailedReportingCategories extends StatefulWidget {
  bool fromHomeScreen;
  bool fromListScreen;
  DetailedReportingCategories({this.fromHomeScreen = false, this.fromListScreen = false});

  @override
  _DetailedReportingCategoriesState createState() => _DetailedReportingCategoriesState();
}

class _DetailedReportingCategoriesState extends State<DetailedReportingCategories> {
  List categories = [];
  List subCategories = [];
  var reporting_category_id = 0;
  var reporting_category_sub_id = 0;
  bool subCategorySelected = false;
  String category = "";
  String title = "Categories";

  _getCategories() async {
    http.Response? response = await ReportService().getCategories(context: context);
    if(response == null) {
      return;
    }

    var results = jsonDecode(response.body);
    setState(() {
      categories = results;
    });
  }

  @override
  void initState() {
    super.initState();
    _getCategories();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: Colors.black,
            image: DecorationImage(
              image: AssetImage("assets/images/home_background.jpeg"),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
            ),
          ),
          child: Column(
            children: [
              SizedBox(height: 40,),
              ListTile(
                leading: IconButton(
                  icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                  onPressed: () {
                    if(!subCategorySelected) {
                      if (widget.fromHomeScreen) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                        Navigator.pop(context);
                      } else if (widget.fromListScreen) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      } else {
                        Navigator.pop(context);
                      }
                    }else {
                      setState(() {
                        subCategorySelected = false;
                        title = "Categories";
                        subCategories = [];
                        reporting_category_sub_id = 0;
                      });
                    }
                  },
                ),
                /**trailing: IconButton(
                  icon: Icon(Icons.backspace_outlined, color: Colors.white,),
                  onPressed: () {
                    setState(() {
                      subCategorySelected = false;
                      subCategories = [];
                      reporting_category_sub_id = 0;
                    });
                  },
                ),*/
              ),
              Container(
                margin: EdgeInsets.only(left: 15, right: 15),
                child: Column(
                  children: [
                    Center(
                      child: Text(
                        "DETAILED REPORT",
                        style: TextStyle(fontSize: 20,decoration: TextDecoration.underline, fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ),
                    Center(
                      child: Text(
                        title,
                        style: TextStyle(fontSize: 14, color: Colors.white),
                      ),
                    ),
                    ListView(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      children: [
                        subCategorySelected ? SizedBox(): Container(
                          //color: Colors.red,
                          height: MediaQuery.of(context).size.height * 0.7,
                          child: GridView.count(
                            shrinkWrap: true,
                            crossAxisCount: 2,
                            children: List.generate(categories.length, (index) {
                              return RoundButton(
                                buttonSize: MediaQuery.of(context).size.height * 0.13,
                                icon: 'assets/images/pothole.png',
                                iconWidth: MediaQuery.of(context).size.width * 0.15,
                                label: categories[index]['category']['category_name'],
                                onPressed: (){
                                  //buttonOptionPressed(option: categories[index]['category']['category_name']);
                                  //print(categories[index]['category_subs']);
                                  setState(() {
                                    subCategorySelected = true;
                                    category = categories[index]['category']['category_name'];
                                    reporting_category_id = categories[index]['category']['category_id'];

                                    subCategories = categories[index]['category_subs'];
                                    title = "Sub Categories";


                                  });
                                },
                              );
                            }),
                          ),
                        ),
                        !subCategorySelected ? SizedBox(): Container(
                          //color: Colors.red,
                          height: MediaQuery.of(context).size.height * 0.7,
                          child: SingleChildScrollView(
                            child: GridView.count(
                              shrinkWrap: true, // use it
                              physics: NeverScrollableScrollPhysics(),
                              // Create a grid with 2 columns. If you change the scrollDirection to
                              // horizontal, this produces 2 rows.
                              crossAxisCount: 2,
                              // Generate 100 widgets that display their index in the List.
                              children: List.generate(subCategories.length, (index) {
                                return Align(
                                  child: RoundButton(
                                    buttonSize: MediaQuery.of(context).size.height * 0.13,
                                    icon: 'assets/images/pothole.png',
                                    iconWidth: MediaQuery.of(context).size.width * 0.15,
                                    label: subCategories[index]['category_sub_name'],
                                    onPressed: (){
                                      //buttonOptionPressed(option: categories[index]['category']['category_name']);
                                      //print(subCategories[index]['category_sub_name']);
                                      var res = {
                                        "category_name": category,
                                        "reporting_category_id": reporting_category_id,
                                        "category_sub_name": subCategories[index]['category_sub_name'],
                                        "reporting_category_sub_id": subCategories[index]['category_sub_id'],
                                      };

                                      Navigator.pop(context, res);
                                    },
                                  ),
                                );
                              }),
                            ),
                          ),
                        ),
                      ],
                    ),
                    /**Container(
                      decoration: BoxDecoration(
                        color: Colors.black,
                      ),
                      child: Column(
                        children: [
                          subCategorySelected ? SizedBox(): GridView.count(
                            shrinkWrap: true, // use it
                            // Create a grid with 2 columns. If you change the scrollDirection to
                            // horizontal, this produces 2 rows.
                            crossAxisCount: 2,
                            // Generate 100 widgets that display their index in the List.
                            children: List.generate(categories.length, (index) {
                              return RoundButton(
                                buttonSize: MediaQuery.of(context).size.height * 0.13,
                                icon: 'assets/images/pothole.png',
                                iconWidth: MediaQuery.of(context).size.width * 0.15,
                                label: categories[index]['category']['category_name'],
                                onPressed: (){
                                  //buttonOptionPressed(option: categories[index]['category']['category_name']);
                                  //print(categories[index]['category_subs']);
                                  setState(() {
                                    subCategorySelected = true;
                                    category = categories[index]['category']['category_name'];
                                    subCategories = categories[index]['category_subs'];
                                  });
                                },
                              );
                            }),
                          ),
                          !subCategorySelected ? SizedBox(): GridView.count(
                            shrinkWrap: true, // use it
                            physics: NeverScrollableScrollPhysics(),
                            // Create a grid with 2 columns. If you change the scrollDirection to
                            // horizontal, this produces 2 rows.
                            crossAxisCount: 2,
                            // Generate 100 widgets that display their index in the List.
                            children: List.generate(subCategories.length, (index) {
                              return RoundButton(
                                buttonSize: MediaQuery.of(context).size.height * 0.13,
                                icon: 'assets/images/pothole.png',
                                iconWidth: MediaQuery.of(context).size.width * 0.15,
                                label: subCategories[index]['category_sub_name'],
                                onPressed: (){
                                  //buttonOptionPressed(option: categories[index]['category']['category_name']);
                                  //print(subCategories[index]['category_sub_name']);
                                  var res = {
                                    "category_name": category,
                                    "category_sub_name": subCategories[index]['category_sub_name']
                                  };

                                  Navigator.pop(context, res);
                                },
                              );
                            }),
                          ),
                        ],
                      ),
                    ),*/

                    /**Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          icon: 'assets/images/pothole.png',
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          label: 'Pothole',
                          onPressed: (){
                            buttonOptionPressed(option: 'Pothole');
                          },
                        ),
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          icon: 'assets/images/pothole.png',
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          label: 'Reinstatement/Trench across the road',
                          onPressed: (){
                            buttonOptionPressed(option: 'Reinstatement/Trench across the road');
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          icon: 'assets/images/pothole.png',
                          label: 'Street name on kerb',
                          onPressed: (){
                            buttonOptionPressed(option: 'Street name on kerb');
                          },
                        ),
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          icon: 'assets/images/pothole.png',
                          label: 'Guardrail',
                          onPressed: (){
                            buttonOptionPressed(option: 'Guardrail');
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          icon: 'assets/images/pothole.png',
                          label: 'Traffic light flashing red',
                          onPressed: (){
                            buttonOptionPressed(option: 'Traffic light flashing red');
                          },
                        ),
                        RoundButton(
                          buttonSize: MediaQuery.of(context).size.height * 0.13,
                          iconWidth: MediaQuery.of(context).size.width * 0.15,
                          icon: 'assets/images/pothole.png',
                          label: 'Blocked stormwater',
                          onPressed: () {
                            buttonOptionPressed(option: 'Road Cracks');
                          },
                        ),
                      ],
                    ),*/
                  ],
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

  void buttonOptionPressed({option}) {
    print(option ?? "No Option Selected");
    saveReport(typeOfReport: option ?? "No Option Selected");
  }

  void saveReport({typeOfReport}) async {
    String userId = await MySharedPreference.get(Constants.USER_ID) ?? "";
    String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);
    print(placeMark);
    //'${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
    http.Response? response = await ReportService().saveReport(
        context: context,
        longitude: position.longitude,
        latitude: position.latitude,
        comment: typeOfReport + " - User Quick Reporting",
        reportingType: typeOfReport,
        personReporting: personReporting,
        userId: userId,
        fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
        locationType: 'My Location',
        country: placeMark.country,
        metropolitan: placeMark.subAdministrativeArea,
        postalCode: placeMark.postalCode,
        province: placeMark.administrativeArea,
        street: placeMark.street,
        suburb: placeMark.subLocality,
        town: placeMark.locality,
        personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString()
    );

    print(response);

    if(response == null) {
        SnackBarWidget.showToast(context: context, message: "Error occurred!");
        return;
    }

    var results = jsonDecode(response.body);
    print(response.body);

    SnackBarWidget.showToast(context: context, message: 'Submitted successfully');
    Navigator.pop(context);
    /**if(response.statusCode == 201) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else if(response.statusCode == 400) {
        SnackBarWidget.showToast(context: context, message: results['message']);
        }else{
        SnackBarWidget.showToast(context: context, message: "Error occurred!");
        }*/
  }
}
