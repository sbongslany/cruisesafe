import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OpenReportTicket extends StatefulWidget {
  const OpenReportTicket({Key? key}) : super(key: key);

  @override
  _OpenReportTicketState createState() => _OpenReportTicketState();
}

class _OpenReportTicketState extends State<OpenReportTicket> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ticket #123'),
      ),
    );
  }
}

