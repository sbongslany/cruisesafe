import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/shared/theme.helper.dart';
import 'package:cruisesafe/ui/views/reports/pic_location_ialog.dart';
import 'package:cruisesafe/ui/widget/camera_modal.dart';
import 'package:cruisesafe/ui/widget/round_button.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;

import 'dart:io';
import 'package:image_picker/image_picker.dart';

import 'detailed_reporting_categories.dart';

class ReportModal extends StatefulWidget {
  bool fromHomeScreen;
  bool fromListScreen;
  ReportModal({this.fromHomeScreen = false, this.fromListScreen = false});
  @override
  _ReportModalState createState() => _ReportModalState();
}

class _ReportModalState extends State<ReportModal> {
  TextEditingController _txtCommentController = new TextEditingController();

  final _gKey = new GlobalKey<ScaffoldState>();

  String myLocation = "";
  String reportType = "Not Selected";
  String reportIcon = "assets/images/pothole.png";
  double longitude = 0.0;
  double latitude = 0.0;
  String personReporting = "";
  String userId = "";
  bool map = false;
  String category = "category not selected", sub_category = "sub_category not selected";
  var _category_id = 0, _category_sub_id = 0;
  String _category_name = "", _category_sub_name = "";


  final ImagePicker _picker = ImagePicker();
  List<XFile>? _imageFileList = [];

  void whatsMyCurrentLocation() async {
    _updateButtonColor('my location');
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    String address = await CruiseSafeLocation.getAddressFromPosition(position);
    setState(() {
      myLocation = address;
      longitude = position.longitude;
      latitude = position.latitude;
    });
  }

  Future<void> loadOtherInfo() async {
    String usrId = await MySharedPreference.get(Constants.USER_ID);
    String ff = await MySharedPreference.get(Constants.USER_FULL_NAME);
    setState(() {
      userId = usrId;
      personReporting = ff;
    });
  }

  void saveReport() async {
    /**

        String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
        Position position = await CruiseSafeLocation.getGeoLocationPosition();
        var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);
        print(placeMark);
        //'${place.street}, ${place.subLocality}, ${place.locality}, ${place.postalCode}, ${place.country}';
        http.Response? response = await ReportService().saveReport(
        context: context,
        longitude: position.longitude,
        latitude: position.latitude,
        comment: typeOfReport + " - User Quick Reporting",
        reportingType: typeOfReport,
        personReporting: personReporting,
        userId: userId,
        fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
        locationType: 'My Location',
        country: placeMark.country,
        metropolitan: placeMark.subAdministrativeArea,
        postalCode: placeMark.postalCode,
        province: placeMark.administrativeArea,
        street: placeMark.street,
        suburb: placeMark.subLocality,
        town: placeMark.locality,
        personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString()
        );

     */
    String personReporting = (await MySharedPreference.get(Constants.USER_FIRST_NAME) ?? "" ) + " " + (await MySharedPreference.get(Constants.USER_LAST_NAME) ?? "" );
    Position position = await CruiseSafeLocation.getGeoLocationPosition();
    var placeMark = await CruiseSafeLocation.getPlaceMarkFromPosition(position);

    http.Response? response = await ReportService().saveReport(
        context: context,
      latitude: latitude,
      longitude: longitude,
      reportingType: reportType,
      comment: _txtCommentController.text,
      personReporting: personReporting,
      userId: userId,
      fullAddress: await CruiseSafeLocation.getAddressFromPosition(position),
      locationType: map ? 'MAP' : 'My Location',
      country: placeMark.country,
      metropolitan: placeMark.subAdministrativeArea,
      postalCode: placeMark.postalCode,
      province: placeMark.administrativeArea,
      street: placeMark.street,
      suburb: placeMark.subLocality,
      town: placeMark.locality,
        personLocationLatLong: position.latitude.toString() + "," + position.longitude.toString(),
      reporting_category_id: _category_id,
      reporting_category_sub_id: _category_sub_id,
      subCategory: sub_category,
      category: category,
      reporting_title: "User Reporting"
    );

    if(response == null) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Network Error occurred!");
      return;
    }

    var results = jsonDecode(response.body);
    print(response.body);
    print(response.statusCode);

    if(response.statusCode == 201) {
      SnackBarWidget.showToast(context: context, message: results['message']);
      Navigator.pop(context);
    }else if(response.statusCode == 400) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['message']);
    }else if(response.statusCode == 417) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else if(response.statusCode == 409) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else if(response.statusCode == 406) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value1']);
    }else{
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
    }
  }

  @override
  void initState() {
    super.initState();
    whatsMyCurrentLocation();
    loadOtherInfo();

    Future.delayed(Duration.zero, () {
      //displayBottomSheet();
      _buildPopForReportingTypeUpDialog();
    });

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _gKey,
      /**appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
            if(reportType == "Not Selected") {
              Navigator.pop(context);
            }
          }, icon: Icon(Icons.arrow_back_ios_outlined),
        ),
        title: Text("Report Issue"),
      ),*/
      body: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Container(
            height: size.height,
            decoration: BoxDecoration(
              color: Colors.black,
              image: DecorationImage(
                image: AssetImage("assets/images/home_background.jpeg"),
                fit: BoxFit.cover,
                colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
              ),
            ),
            child: Column(
              children: [
                SizedBox(height: 40,),
                ListTile(
                  leading: IconButton(
                    icon: Icon(Icons.arrow_back_sharp, color: Colors.white,),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                ),
                Center(
                  child: Text(
                    " REPORT ",
                    style: TextStyle(fontSize: 20,decoration: TextDecoration.underline, fontWeight: FontWeight.bold, color: Constants.getPrimaryColor()),
                  ),
                ),
                Center(
                  child: Text(
                    " ISSUE",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                SizedBox(height: 20,),
                Expanded(
                  child: SingleChildScrollView(
                    child: Container(
                      //color: Colors.red,
                      margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                      child: Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              //displayBottomSheet();
                              _buildPopForReportingTypeUpDialog();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(reportIcon, width: MediaQuery.of(context).size.width * 0.09,),
                                      SizedBox(width: 20,),
                                      Text(reportType, style: TextStyle(color: Constants.lblColor, fontSize: 18, fontWeight: FontWeight.bold),)
                                    ],
                                  ),
                                  Icon(Icons.keyboard_arrow_down, color: Colors.grey,)
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 20,),
                          GestureDetector(
                            onTap: () {
                              //displayBottomSheet();
                              _buildPopForReportingTypeUpDialog();
                            },
                            child: Container(
                              padding: const EdgeInsets.all(3.0),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Image.asset(reportIcon, width: MediaQuery.of(context).size.width * 0.09,),
                                      SizedBox(width: 20,),
                                      Text(sub_category, style: TextStyle(color: Constants.lblColor, fontSize: 18, fontWeight: FontWeight.bold),)
                                    ],
                                  ),
                                  Icon(Icons.keyboard_arrow_down, color: Colors.grey,)
                                ],
                              ),
                            ),
                          ),
                          SizedBox(height: 20,),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.47,
                                  child: TextButton(
                                    onPressed: () {
                                      whatsMyCurrentLocation();
                                    },
                                    style: TextButton.styleFrom(
                                      backgroundColor: !map ? Colors.black12 : Constants.getPrimaryColor(),
                                      padding: EdgeInsets.all(10.0),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.my_location_sharp,
                                          color: !map ? Constants.getPrimaryColor() : Colors.black12,
                                        ),
                                        SizedBox(width: 5), // Add some space between icon and text
                                        Text(
                                          "My Location",
                                          style: TextStyle(
                                            color: !map ? Constants.getPrimaryColor() : Colors.black12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: MediaQuery.of(context).size.width * 0.47,
                                  child: TextButton(
                                    onPressed: () {
                                      _selectMap(context);
                                    },
                                    style: TextButton.styleFrom(
                                      backgroundColor: map ? Colors.black12 : Constants.getPrimaryColor(),
                                      padding: EdgeInsets.all(10.0),
                                    ),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.location_on_outlined,
                                          color: map ? Constants.getPrimaryColor() : Colors.black12,
                                        ),
                                        SizedBox(width: 5), // Add some space between icon and text
                                        Text(
                                          "Map",
                                          style: TextStyle(
                                            color: map ? Constants.getPrimaryColor() : Colors.black12,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(height: 20,),
                          Container(
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                              ),
                              child: Text(myLocation, style: TextStyle(color: Constants.getPrimaryColor(),),)
                          ),
                          SizedBox(height: 20,),
                          txtCommentWidget(),
                          SizedBox(height: 20,),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              for(int i = 0; i < _imageFileList!.length; i++)
                                Row(
                                  children: [
                                    Image.file(File(_imageFileList![i].path), width: MediaQuery.of(context).size.width * 0.15,),
                                    SizedBox(width: MediaQuery.of(context).size.width * 0.02,),
                                  ],
                                ),
                              _imageFileList!.length > 2 ? SizedBox() : GestureDetector(
                                  onTap: () {
                                    FocusScope.of(context).unfocus(); //clear keyboard
                                    _buildCameraPopupDialog(context);
                                    //openDialog();
                                  },
                                  child: Image.asset('assets/images/img_pic.png', height: size.height * 0.08,)
                              ),
                            ],
                          ),
                          SizedBox(height: 40,),
                          SizedBox(
                            width: MediaQuery.of(context).size.width,
                            child: TextButton(
                              onPressed: () {
                                saveReport();
                              },
                              style: TextButton.styleFrom(
                                backgroundColor: Constants.getPrimaryColor(),
                                padding: EdgeInsets.all(10.0),
                              ),
                              child: Text(
                                'SUBMIT',
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                          ),
                          /**Expanded(
                              child: Align(
                              alignment: FractionalOffset.bottomCenter,
                              child: Padding(
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: SizedBox(
                              width: MediaQuery.of(context).size.width * 0.9,
                              child: FlatButton(
                              onPressed: () => { saveReport() },
                              color:Constants.getPrimaryColor(),
                              padding: EdgeInsets.all(10.0),
                              child: Text('SUBMIT')
                              ),
                              ),//Your widget here,
                              ),
                              ),
                              ),*/
                        ],
                      ),
                    ),
                  ),
                ),
                /**Container(
                    //color: Colors.red,
                    margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                    child: Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            //displayBottomSheet();
                            _buildPopForReportingTypeUpDialog();
                          },
                          child: Container(
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Image.asset(reportIcon, width: MediaQuery.of(context).size.width * 0.09,),
                                    SizedBox(width: 20,),
                                    Text(reportType, style: TextStyle(color: Constants.lblColor, fontSize: 18, fontWeight: FontWeight.bold),)
                                  ],
                                ),
                                Icon(Icons.keyboard_arrow_down, color: Colors.grey,)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        GestureDetector(
                          onTap: () {
                            //displayBottomSheet();
                            _buildPopForReportingTypeUpDialog();
                          },
                          child: Container(
                            padding: const EdgeInsets.all(3.0),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Image.asset(reportIcon, width: MediaQuery.of(context).size.width * 0.09,),
                                    SizedBox(width: 20,),
                                    Text(sub_category, style: TextStyle(color: Constants.lblColor, fontSize: 18, fontWeight: FontWeight.bold),)
                                  ],
                                ),
                                Icon(Icons.keyboard_arrow_down, color: Colors.grey,)
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.all(Radius.circular(5.0)),
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.47,
                                child: FlatButton(
                                  onPressed: () => { whatsMyCurrentLocation() },
                                  color: !map ? Colors.black12  : Constants.getPrimaryColor(),
                                  padding: EdgeInsets.all(10.0),
                                  child: Row( // Replace with a Row for horizontal icon + text
                                    children: <Widget>[
                                      Icon(Icons.my_location_sharp),
                                      Text("My Location", style: TextStyle(color: !map ? Constants.getPrimaryColor() : Colors.black12,),)
                                    ],
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: MediaQuery.of(context).size.width * 0.47,
                                child: FlatButton(
                                  onPressed: () => {
                                    _selectMap(context)
                                  },
                                  color: map ? Colors.black12 : Constants.getPrimaryColor(),
                                  padding: EdgeInsets.all(10.0),
                                  child: Row( // Replace with a Row for horizontal icon + text
                                    children: <Widget>[
                                      Icon(Icons.location_on_outlined),
                                      Text("Map", style: TextStyle(color: map ? Constants.getPrimaryColor() : Colors.black12,),)
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 20,),
                        Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.all(Radius.circular(5.0)),
                            ),
                            child: Text(myLocation, style: TextStyle(color: Constants.getPrimaryColor(),),)
                        ),
                        SizedBox(height: 20,),
                        txtCommentWidget(),
                        SizedBox(height: 20,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            for(int i = 0; i < _imageFileList!.length; i++)
                              Row(
                                children: [
                                  Image.file(File(_imageFileList![i].path), width: MediaQuery.of(context).size.width * 0.15,),
                                  SizedBox(width: MediaQuery.of(context).size.width * 0.02,),
                                ],
                              ),
                            _imageFileList!.length > 2 ? SizedBox() : GestureDetector(
                                onTap: () {
                                  _buildCameraPopupDialog(context);
                                  //openDialog();
                                },
                                child: Image.asset('assets/images/img_pic.png', height: size.height * 0.08,)
                            ),
                          ],
                        ),
                        SizedBox(height: 40,),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: FlatButton(
                              onPressed: () => { saveReport() },
                              color:Constants.getPrimaryColor(),
                              padding: EdgeInsets.all(10.0),
                              child: Text('SUBMIT')
                          ),
                        ),
                        /**Expanded(
                          child: Align(
                            alignment: FractionalOffset.bottomCenter,
                            child: Padding(
                              padding: EdgeInsets.only(bottom: 20.0),
                              child: SizedBox(
                                width: MediaQuery.of(context).size.width * 0.9,
                                child: FlatButton(
                                    onPressed: () => { saveReport() },
                                    color:Constants.getPrimaryColor(),
                                    padding: EdgeInsets.all(10.0),
                                    child: Text('SUBMIT')
                                ),
                              ),//Your widget here,
                            ),
                          ),
                        ),*/
                      ],
                    ),
                  ),*/
              ],
            ),
          ),
      ),
    );
  }

  Widget txtCommentWidget() {
    return TextFormField(
      style: TextStyle(color: Colors.orangeAccent),
      maxLines: 4,
      controller: _txtCommentController,
      decoration: ThemeHelper().textInputDecoration2('Comment', 'Enter your comment...'),
      validator: (val) {
        if (val!.length == 0) {
          return "Comment cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  _updateButtonColor(String type){
    bool ff = false;
    if(type == 'MAP') {
      ff = true;
    }
    setState(() {
      map = ff;
    });
  }

  void _selectMap(BuildContext context) async {

    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) => const PicLocationDialog()),
    );

    print(result);

    if(result != null) {
      setState(() {
        myLocation = result['addressLocation'];
        latitude = result['latitude'];
        longitude = result['longitude'];
        _updateButtonColor('MAP');
      });
    }
  }

  //https://api.flutter.dev/flutter/material/showModalBottomSheet.html
  void displayBottomSheet()
  {
    /**_gKey.currentState!.showBottomSheet((context)
    {
      return Container(
          decoration: new BoxDecoration(
              color: Colors.grey.shade300,
          ),
          height: MediaQuery.of(context).size.height * 0.9,
          //width: double.infinity,
          width: double.infinity,
          //color: Colors.grey.shade200,
          //alignment: Alignment.center,
          child: Container(
            margin: EdgeInsets.all(20),
            child: Column(
              children: [
                //Image(image: AssetImage("assets/images/jra_splash2.png"), width: MediaQuery.of(context).size.width * 0.3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/pothole.png'),
                      label: 'Pothole',
                      onPressed: (){
                        buttonOptionPressed(option: 'Pothole', icon: 'assets/images/pothole.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/traffic_lights.png'),
                      label: 'Traffic Light',
                      onPressed: (){
                        buttonOptionPressed(option: 'Traffic Light', icon: 'assets/images/traffic_lights.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/bad_road.png'),
                      label: 'Bad Road',
                      onPressed: (){
                        buttonOptionPressed(option: 'Bad Road', icon: 'assets/images/bad_road.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/drainage.png'),
                      label: 'Water Drain',
                      onPressed: (){
                        buttonOptionPressed(option: 'Water Drain', icon: 'assets/images/drainage.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/manhole.png'),
                      label: 'Manhole Cover',
                      onPressed: (){
                        buttonOptionPressed(option: 'Manhole Cover', icon: 'assets/images/manhole.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      icon: Image.asset('assets/images/crack.png'),
                      label: 'Road Cracks',
                      onPressed: (){
                        buttonOptionPressed(option: 'Road Cracks', icon: 'assets/images/crack.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width * 0.9,
                  child: RaisedButton(
                    color: Colors.grey,
                    onPressed: () {
                      buttonOptionPressed(option: 'Road Cracks', icon: 'assets/images/crack.png');
                      Navigator.of(context).pop();
                    },
                    child: Text("OTHER"),
                  ),
                ),
              ],
            ),
          )
      );
    }
    );*/
  }

  void buttonOptionPressed({option, icon, subCategory, category_id, category_sub_id}) {
    //print(option ?? "No Option Selected");
    setState(() {
      reportType = option;
      reportIcon = icon;
      category = option;
      sub_category = subCategory;
      _category_id = category_id;
      _category_sub_id = category_sub_id;
    });
    //saveReport(typeOfReport: option ?? "No Option Selected");
    //Navigator.of(context).pop();
  }

   _buildCameraPopupDialog(BuildContext context) {
     // set up the button
     Widget okButton = TextButton(
       child: Text("OK"),
       onPressed: () {
         Navigator.pop(context);
       },
     );

     // set up the AlertDialog
     AlertDialog alert = AlertDialog(
       //title: Text("Choose"),
       backgroundColor: Colors.transparent,
       content: Container(
         height: MediaQuery.of(context).size.height * 0.2,
         decoration: BoxDecoration(
           border: Border.all(color: Colors.orangeAccent),
           color: Colors.black,
           image: DecorationImage(
             image: AssetImage("assets/images/home_background.jpeg"),
             fit: BoxFit.cover,
             colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
           ),
         ),
         child: Column(
           children: [
             //Image.asset('assets/images/jra_splash2.png', width: MediaQuery.of(context).size.width * 0.4,),
             ListTile(
               onTap: () { Navigator.pop(context); _getFromCamera(); },
               leading: Icon(Icons.camera_alt_outlined, color: Constants.iconColor,),
               title: Text('Take a picture', style: TextStyle(color: Colors.orangeAccent),),
             ),
             Expanded(child: Divider()),
             ListTile(
               onTap: () { Navigator.pop(context); _getFromGallery(); },
               leading: Icon(Icons.image, color: Constants.iconColor,),
               title: Text('Choose from Library', style: TextStyle(color: Colors.orangeAccent),),
             ),
             /**ElevatedButton(
               onPressed: () { Navigator.pop(context); },
               child: Text('CANCEL', style: TextStyle(color: Constants.btnTextColor),),
               style: ElevatedButton.styleFrom(
                 primary: Constants.btnColor,
                 minimumSize: Size(double.infinity, MediaQuery.of(context).size.height * 0.06), // double.infinity is the width and 30 is the height
               ),
             ),*/
           ],
         ),
       ),
     );

     // show the dialog
     showDialog(
       context: context,
       builder: (BuildContext context) {
         return alert;
       },
     );
  }

  void openDialog() {
    Navigator.of(context).push(new MaterialPageRoute<Null>(
        builder: (BuildContext context) {
          return new CameraModal();
        },
        fullscreenDialog: true));
  }


  _buildPopForReportingTypeUpDialog() async {
    var results = await Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => DetailedReportingCategories(fromHomeScreen: widget.fromHomeScreen, fromListScreen: widget.fromListScreen,)));
    print(results);
    buttonOptionPressed(
        option: results['category_name'],
        subCategory: results['category_sub_name'],
        icon: 'assets/images/pothole.png',
        category_id: results['reporting_category_id'],
        category_sub_id: results['reporting_category_sub_id']
    );
    // set up the button
    /**Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      //title: Text("Choose"),
      scrollable: true,
      content: Container(
          decoration: new BoxDecoration(
            //color: Colors.grey.shade300,
          ),
          height: MediaQuery.of(context).size.height * 0.75,
          //width: double.infinity,
          width: double.infinity,
          //color: Colors.grey.shade200,
          //alignment: Alignment.center,
          child: Container(
            child: Column(
              children: [
                Image(image: AssetImage("assets/images/jra_splash2.png"), width: MediaQuery.of(context).size.width * 0.3,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      icon: 'assets/images/pothole.png',
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      label: 'Pothole',
                      onPressed: (){
                        buttonOptionPressed(option: 'Pothole', icon: 'assets/images/pothole.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      icon: 'assets/images/traffic_lights.png',
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      label: 'Traffic Light',
                      onPressed: (){
                        buttonOptionPressed(option: 'Traffic Light', icon: 'assets/images/traffic_lights.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/bad_road.png',
                      label: 'Bad Road',
                      onPressed: (){
                        buttonOptionPressed(option: 'Bad Road', icon: 'assets/images/bad_road.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.13,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/drainage.png',
                      label: 'Water Drain',
                      onPressed: (){
                        buttonOptionPressed(option: 'Water Drain', icon: 'assets/images/drainage.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/manhole.png',
                      label: 'Manhole Cover',
                      onPressed: (){
                        buttonOptionPressed(option: 'Manhole Cover', icon: 'assets/images/manhole.png');
                      },
                    ),
                    RoundButton(
                      buttonSize: MediaQuery.of(context).size.height * 0.14,
                      iconWidth: MediaQuery.of(context).size.width * 0.15,
                      icon: 'assets/images/crack.png',
                      label: 'Road Cracks',
                      onPressed: (){
                        buttonOptionPressed(option: 'Road Cracks', icon: 'assets/images/crack.png');
                      },
                    ),
                  ],
                ),
                SizedBox(height: 20,),
                ButtonTheme(
                  minWidth: MediaQuery.of(context).size.width * 0.9,
                  child: RaisedButton(
                    color: Colors.grey,
                    onPressed: () {
                      buttonOptionPressed(option: 'Other', icon: 'assets/images/spot.png');
                      //Navigator.of(context).pop();
                    },
                    child: Text("OTHER"),
                  ),
                ),
              ],
            ),
          )
      ),
    );

    // show the dialog
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );*/
  }

  Widget _previewImages() {
    if (_imageFileList != null) {
      return ListView.builder(
        key: UniqueKey(),
        itemBuilder: (context, index) {
          return Image.file(File(_imageFileList![index].path));
        },
        itemCount: _imageFileList!.length,
      );
    }else {
      return const Text(
        'You have not yet picked an image.',
        textAlign: TextAlign.center,
      );
    }
  }




  /// Get from gallery
  _getFromGallery() async {
    try {
      final pickedFileList = await _picker.pickMultiImage(
        maxWidth: 1800,
        maxHeight: 1800,
        //imageQuality: quality,
      );
      setState(() {
        _imageFileList = pickedFileList;
      });
    } catch (e) {
      setState(() {
        //_pickImageError = e;
      });
    }
  }

  /// Get from camera
  _getFromCamera() async {
    var pickedFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 1800,
      maxHeight: 1800,
    );

    if (pickedFile != null) {
      File imageFile = File(pickedFile.path);
      setState(() {
        _imageFileList!.add(pickedFile);
      });

    }
  }


}