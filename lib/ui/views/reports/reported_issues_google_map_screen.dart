import 'dart:async';
import 'dart:convert';
import 'package:cruisesafe/core/location/cruise_safe_location.dart';
import 'package:cruisesafe/core/services/report.service.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:location/location.dart';
import 'package:http/http.dart' as http;

class ReportedIssuesGoogleMapScreen extends StatefulWidget {
  @override
  State<ReportedIssuesGoogleMapScreen> createState() => ReportedIssuesGoogleMapScreenState();
}

class ReportedIssuesGoogleMapScreenState extends State<ReportedIssuesGoogleMapScreen> {
  //Completer<GoogleMapController> _controller = Completer();
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  bool currentMapLoaded = false;
  int _markerIdCounter = 0;

  LatLng _initialcameraposition = LatLng(-30.745048104080578, 24.91135586053133);

  late GoogleMapController _controller;
  Location _location = Location();
  List localReports = [];

  void _onMapCreated(GoogleMapController _cntlr)
  {
    _controller = _cntlr;
    _location.onLocationChanged.listen((l) {
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(l.latitude ?? 0.0, l.longitude ?? 0.0),zoom: 15),
        ),
      );
    });
  }

  _updateLocation() async {
    Position position = await CruiseSafeLocation.getGeoLocationPosition();

    double lat = position.latitude;
    double longi = position.longitude;
    setState(() {
      _initialcameraposition = LatLng(lat, longi);
      _controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: LatLng(lat, longi),zoom: 17),
        ),
      );
      if(!currentMapLoaded) {
        currentMapLoaded = true;
      }
    });
    _loadReportsAroundYou();
  }

  void _addMarker(lat, longi, markerIdV, markerTitle) {
    final int markerCount = markers.length;

    if (markerCount == 5) {
      return;
    }

    final String markerIdVal = 'marker_id_';
    _markerIdCounter++;
    final MarkerId markerId = MarkerId(markerIdV);

    final Marker marker = Marker(
      markerId: markerId,
      position: LatLng(
        lat,
        longi,
      ),
      infoWindow: InfoWindow(title: markerTitle, snippet: '*', onTap: (){}),
      //onTap: () => _onMarkerTapped(markerId),
      //onDragEnd: (LatLng position) => _onMarkerDragEnd(markerId, position),
      //onDrag: (LatLng position) => _onMarkerDrag(markerId, position),
    );

    setState(() {
      markers[markerId] = marker;
    });
  }

  void _loadReportsAroundYou() async {
    try {
      http.Response? response = await ReportService().getReportsAroundYou(context: context);
      //print(response?.body);
      if(response == null) {
        SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Sorry, could not load road issues around you.");
        return;
      }

      //print(response.body);
      var json = jsonDecode(response.body);
      //print(json);
      setState(() {
        localReports = jsonDecode(response.body);
      });

      for(int k = 0; k < localReports.length; k++) {
        print('K$k');
        _addMarker( double.parse(localReports[k]['ticket_latitude']), double.parse(localReports[k]['ticket_longitude']), 'makerId$k', localReports[k]['ticket_category']);
      }
      //_addMarker(lat, longi)

    }catch(e){
      print(e);
    }
    finally{

    }
  }

  @override
  void initState() {
    super.initState();
    _updateLocation();
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('Reported Issues'),
      ),
      body: Container(
        child: currentMapLoaded ? loadCurrentMap() : loadInitMap(),
      )
    );
  }

  Widget loadInitMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: _initialcameraposition, zoom: 5),
      mapType: MapType.normal,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
    );
  }

  Widget loadCurrentMap() {
    return GoogleMap(
      initialCameraPosition: CameraPosition(target: _initialcameraposition, zoom: 14),
      mapType: MapType.hybrid,
      onMapCreated: _onMapCreated,
      myLocationEnabled: true,
      markers: Set<Marker>.of(markers.values),
    );
  }

}