import 'dart:convert';

import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/services/user.service.dart';
import 'package:cruisesafe/ui/views/home/home_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/google_login_controller.dart';
import 'package:cruisesafe/ui/views/authentication/registration.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:cruisesafe/ui/widget/social_button_widget.dart';
import 'package:cruisesafe/ui/widget/submit_button.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:twitter_login/twitter_login.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

//import 'dart:convert';

//import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';


//import 'package:http/http.dart' as http;
//import 'package:provider/provider.dart';

import 'login_screen.dart';

class LandingScreen extends StatefulWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  _LandingScreenState createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          decoration: Constants.backGroundDeco(img: "assets/images/login_signup_background.jpeg"),
          child: SafeArea(
            child: SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      children: [
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          'Create an Account',style: TextStyle(fontSize: 40, color: Colors.white),
                        ),
                        Text('Sign up with your social media or email account',style: TextStyle(fontSize: 15, color: Colors.white)),
                        SizedBox(
                          height: 60,
                        ),
                        SubmitButton(
                          label: 'Register',
                          onPressed: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => RegistrationScreen()));
                          },
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        SubmitButton(
                          label: 'Login',
                          onPressed: (){
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => LoginScreen()));
                          },
                        ),
                        SizedBox(
                          height: 80,
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        //Text('--------- OR ----------'),
                        SizedBox(
                          height: 30,
                        ),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        SocialButtonWidget(text: 'Facebook', onPress: () { faceBookLogin(context); }, backgroundColor: Colors.blue, icon: Icon(FontAwesomeIcons.facebookF),),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        SocialButtonWidget(text: 'Twitter', onPress: () { twitterLogin(context); }, backgroundColor: Colors.lightBlueAccent, icon: Icon(FontAwesomeIcons.twitter),),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        SocialButtonWidget(text: 'Google', onPress: () { googleLogin(); }, backgroundColor: Colors.redAccent, icon: Icon(FontAwesomeIcons.google),),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
      ),
    );
  }

  void faceBookLogin(BuildContext context) async {
    try{
      final LoginResult result = await FacebookAuth.instance.login(); // by default we request the email and the public profile
      // or FacebookAuth.i.login()
      if (result.status == LoginStatus.success) {
        // you are logged
        //final AccessToken accessToken = result.accessToken!;
        print(result.accessToken);
        print(result.message);
        print(result.status);
        String url = 'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${result.accessToken!.token.toString()}';
        print(url);
        var graphResponse = await http.get(Uri.parse(url));
        var profile = json.decode(graphResponse.body);
        print(profile.toString());
        //{name: Nhlanhla Mkhize, first_name: Nhlanhla, last_name: Mkhize, email: mbmkhize22@gmail.com, id: 5157355427619401}
        submit(
            email: profile['email'],
            name: profile['first_name'],
            surname: profile['last_name'],
            picture: "",
            registered_facebook_id: profile['id'],
            registered_login_with: 'Facebook'
        );
      } else {
        print(result.status);
        print(result.message);
      }
    }catch(e) {
      print(e);
    }
    //SnackBarWidget.showToast(context: context, message: 'No Implemented Yet');
  }

  void twitterLogin(BuildContext context) async {
    try {
      final twitterLogin = TwitterLogin(
          apiKey: "ITqtDPVkTHYfGUaqXtmHT79XP",
          apiSecretKey: "kCdDV0PLAPNBLfUwVljq3XA3IzWCdOHsmF2ZVyMAV1UA9GSE69",
          redirectURI: "twitterkit-ITqtDPVkTHYfGUaqXtmHT79XP://",
      );

      print(twitterLogin.apiKey);
      print(twitterLogin.apiSecretKey);
      print(twitterLogin.redirectURI);
      final authResult = await twitterLogin.loginV2(forceLogin: true);

      switch (authResult.status) {
        case TwitterLoginStatus.loggedIn:
          print('success');
          print(authResult.user!.email);
          print(authResult.user!.name);
          print(authResult.user!.thumbnailImage);
          print(authResult.user!.screenName);
          print(authResult.user.toString());
          submit(
              email: authResult.user!.screenName,
              name: authResult.user!.name,
              surname: "",
              picture: authResult.user!.thumbnailImage,
              registered_twitter_id: authResult.user!.screenName,
              registered_login_with: 'Twitter'
          );
          break;
        case TwitterLoginStatus.cancelledByUser:
          print('cancelled');
          break;
        case TwitterLoginStatus.error:
          print('error');
          print('STATUS: ${authResult.status}');
          print('TO_STRING: ${authResult.toString()}');
          print('MESSAGE: ${authResult.errorMessage}');
          SnackBarWidget.showToast(context: context, message: authResult.errorMessage);
          break;

      }
    }catch(e) {
      print(e);
    }
  }

  /** loginCheck() {
      return Consumer<GoogleSignInController>(builder: (context, model, child) {
      if(model.googleAccount != null){

      }else{
      return
      }
      });
      } */

  void googleLogin() async {
    GoogleSignInController controller = new GoogleSignInController();
    await controller.login();
    //print(controller.googleAccount.toString());
    submit(
     email: controller.googleAccount!.email,
      name: controller.googleAccount!.displayName,
      picture: controller.googleAccount!.photoUrl,
        google_id: controller.googleAccount!.id,
      registered_login_with: 'Google'
    );
  }

  submit({name, surname, email, picture, registered_twitter_id, google_id, registered_login_with, registered_facebook_id}) async {


    http.Response? res = await UserService().signUp(
      context: context,
      firstName: name,
          surname: surname,
          email: email,
      google_id: google_id,
        isSocialMedia: true,
        registered_login_with: registered_login_with,
      registered_facebook_id: registered_facebook_id,
      registered_twitter_id: registered_twitter_id
    );

    if(res == null) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: this, message: "Error occurred!");
      return;
    }

    print(res.statusCode);
    print(res.body);
    var results = jsonDecode(res.body);

    if(res.statusCode == 200) {
      //print(res.body);
      //SnackBarWidget.showSnackBar(color: Colors.blueGrey, context: context, message: results['message']);
      MySharedPreference.clear(); //clear all local storage
      await MySharedPreference.save(key: Constants.USER_EMAIL, value: email ?? "");
      await MySharedPreference.save(key: Constants.USER_FULL_NAME, value: name ?? "");
      await MySharedPreference.save(key: Constants.USER_FIRST_NAME, value: name ?? "");
          await MySharedPreference.save(key: Constants.USER_LAST_NAME, value: surname ?? "");
          await MySharedPreference.save(key: Constants.USER_MOBILE, value: "");
      await MySharedPreference.save(key: Constants.LOGIN_TYPE, value: registered_login_with ?? "Account");
      await MySharedPreference.save(key: Constants.USER_ID, value: '' + results['value']);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen(
          int.parse(results['value']),
          email,
          '',
          '',
          ''
      )));
    }else if(res.statusCode == 400) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else{
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
    }
  }

}
