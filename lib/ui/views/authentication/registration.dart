import 'dart:convert';

import 'package:cruisesafe/core/config/constants.dart';
import 'package:cruisesafe/core/config/shared_pref.dart';
import 'package:cruisesafe/core/services/user.service.dart';
import 'package:cruisesafe/ui/shared/theme.helper.dart';
import 'package:cruisesafe/ui/views/home/home_screen.dart';
import 'package:cruisesafe/ui/widget/snackBar.widget.dart';
import 'package:cruisesafe/ui/widget/submit_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class RegistrationScreen extends StatefulWidget {
  const RegistrationScreen({Key? key}) : super(key: key);

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}

class _RegistrationScreenState extends State<RegistrationScreen> {

  TextEditingController _txtNameController = new TextEditingController();
  TextEditingController _txtSurnameController = new TextEditingController();
  TextEditingController _txtMobileController = new TextEditingController();
  TextEditingController _txtEmailController = new TextEditingController();
  TextEditingController _txtPassword1Controller = new TextEditingController();
  TextEditingController _txtPassword2Controller = new TextEditingController();
  TextEditingController _txtAddressController = new TextEditingController();

  bool checkboxValue = false;

  GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      /**appBar: AppBar(
        title: Text("Register"),
      ),*/
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: Constants.backGroundDeco(img: "assets/images/register_background.jpeg"),
          child: Column(
            children: [
              SizedBox(height: 50,),
              ListTile(leading: IconButton(icon: Icon(Icons.arrow_back_outlined, color: Constants.btnBackColor,), onPressed: () {Navigator.pop(context); },),),
              Container(
                  margin: EdgeInsets.all(Constants.WIDGET_ALL_SIDES_MARGINS),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      children: [
                        //txtNameWidget(),
                        SizedBox(height: 20,),
                        Text('REGISTER', style: TextStyle(color: Constants.lblColor2, fontSize: 25, decoration: TextDecoration.underline,),),
                        SizedBox(height: 30,),
                        //txtSurnameWidget(),
                        //SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        //txtMobileWidget(),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        txtEmailWidget(),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        txtPassword1Widget(),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        txtPassword2Widget(),
                        //SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        //txtAddressWidget(),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        termsAndCondition(),
                        SizedBox(height: Constants.SPACE_BETWEEN_TXT,),
                        SubmitButton(label: "REGISTER", onPressed: () { submit(); },),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
      )
    );
  }

  submit() async {

    if (!_formKey.currentState!.validate()) {
      return;
    }

    if(!checkboxValue){
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Accept Terms & Conditions");
      return;
    }

    http.Response? res = await UserService().signUp(
        context: context,
        firstName: _txtNameController.text,
        surname: _txtSurnameController.text,
        email: _txtEmailController.text,
        mobile: _txtMobileController.text,
        password: _txtPassword1Controller.text
    );

    if(res == null) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
      return;
    }

    print(res.body);
    var results = jsonDecode(res.body);

    if(res.statusCode == 201) {
      //print(res.body);
      //SnackBarWidget.showSnackBar(color: Colors.blueGrey, context: context, message: results['message']);
      MySharedPreference.clear(); //clear all local storage
      await MySharedPreference.save(key: Constants.USER_EMAIL, value: _txtEmailController.text);
      await MySharedPreference.save(key: Constants.USER_FULL_NAME, value: _txtNameController.text + " " + _txtSurnameController.text);
      await MySharedPreference.save(key: Constants.USER_FIRST_NAME, value: _txtNameController.text);
      await MySharedPreference.save(key: Constants.USER_LAST_NAME, value: _txtSurnameController.text);
      await MySharedPreference.save(key: Constants.USER_MOBILE, value: _txtMobileController.text);
      await MySharedPreference.save(key: Constants.LOGIN_TYPE, value: "Account");
      await MySharedPreference.save(key: Constants.USER_ID, value: '' + results['value']);
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeScreen(
           int.parse(results['value']),
          _txtEmailController.text,
          '',
          _txtSurnameController.text,
          _txtNameController.text
      )));
    }else if(res.statusCode == 400) {
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: results['value']);
    }else{
      SnackBarWidget.showSnackBar(color: Colors.red, context: context, message: "Error occurred!");
    }
  }

  Widget txtNameWidget() {
    return TextFormField(
      controller: _txtNameController,
      decoration: ThemeHelper().textInputDecoration('Name', 'Enter your name'),
      validator: (val) {
        if (val!.length == 0) {
          return "Name cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtSurnameWidget() {
    return TextFormField(
      controller: _txtSurnameController,
      decoration: ThemeHelper().textInputDecoration('Surname', 'Enter your surname'),
      validator: (val) {
        if (val!.length == 0) {
          return "Surname cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtMobileWidget() {
    return TextFormField(
      controller: _txtMobileController,
      keyboardType: TextInputType.number,
      decoration: ThemeHelper().textInputDecoration('Mobile Number', 'Enter your mobile number'),
      validator: (val) {
        if (val!.length == 0) {
          return "Mobile number cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtPassword1Widget() {
    return TextFormField(
      controller: _txtPassword1Controller,
      obscureText: true,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration2('PASSWORD', 'Enter your password'),
      validator: (val) {
        if (val!.length == 0) {
          return "Password cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtPassword2Widget() {
    return TextFormField(
      controller: _txtPassword2Controller,
      obscureText: true,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration2('CONFIRM PASSWORD', 'Enter your password'),
      validator: (val) {
        if (val!.length == 0) {
          return "Password cannot be empty";
        } else if (_txtPassword1Controller.text != _txtPassword2Controller.text) {
          return "Passwords doesn't match";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtEmailWidget() {
    return TextFormField(
      controller: _txtEmailController,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration2('EMAIL ADDRESS/PHONE NUMBER', 'Enter email address or phone number'),
      validator: (val) {
        String pattern =
            r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]"
            r"{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]"
            r"{0,253}[a-zA-Z0-9])?)*$";
        RegExp regex = RegExp(pattern);
        if (val!.length == 0) {
          return "Email address cannot be empty";
        }else if (!regex.hasMatch(val)) {
          return "Invalid email address";
        } else {
          return null;
        }
      },
    );
  }

  Widget txtAddressWidget() {
    return TextFormField(
      controller: _txtAddressController,
      cursorColor: Constants.cursorColor,
      style: TextStyle(color: Constants.txtTextBoxColor),
      decoration: ThemeHelper().textInputDecoration('ADDRESS', 'Enter address'),
      validator: (val) {
        if (val!.length == 0) {
          return "Address cannot be empty";
        } else {
          return null;
        }
      },
    );
  }

  Widget termsAndCondition() {
    return  Row(
      children: <Widget>[
        Theme(
          data: Theme.of(context).copyWith(
            unselectedWidgetColor: Colors.white,
          ),
          child: Checkbox(
              value: checkboxValue,
              checkColor: Constants.txtTextBoxColor,
              activeColor: Constants.lblColor,
              onChanged: (value) {
                setState(() {
                  checkboxValue = value!;
                });
              }),
        ),
        Text(
          "Agree To Terms and Conditions",
          style: TextStyle(color: Constants.btnColor, fontSize: 17, fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

}
