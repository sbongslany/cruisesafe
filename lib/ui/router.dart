import 'package:cruisesafe/ui/views/authentication/landing_screen.dart';
import 'package:cruisesafe/ui/views/authentication/registration.dart';
import 'package:cruisesafe/ui/views/reports/open_report_ticket.dart';
import 'package:cruisesafe/ui/views/home/home_screen.dart';
import 'package:cruisesafe/ui/views/introduction_screens/splash_screen.dart';
import 'package:cruisesafe/ui/views/introduction_screens/welcome_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Routers {
  /// App start up (loading) page
  static const String startUp = '/';

  static const String loginScreen = '/loginScreen';

  static const String registrationScreen = '/registrationScreen';

  static const String homeScreen = '/homeScreen';

  static const String welcomeScreen = '/welcomeScreen';

  static const String detailedReporting = '/detailedReporting';

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case startUp:
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case loginScreen:
        return MaterialPageRoute(builder: (_) => LandingScreen());
      case registrationScreen:
        return MaterialPageRoute(builder: (_) => RegistrationScreen());
      /**case homeScreen:
        return MaterialPageRoute(builder: (_) => HomeScreen());*/
      case welcomeScreen:
        return MaterialPageRoute(builder: (_) => WelcomeScreen());
      case detailedReporting:
        return MaterialPageRoute(builder: (_) => OpenReportTicket());
      default:
        throw UnimplementedError();
    }
  }
}
