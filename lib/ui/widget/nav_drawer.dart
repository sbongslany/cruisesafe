import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(boxShadow: [
        BoxShadow(
            color: Theme.of(context).primaryColor.withOpacity(0.20),
            blurRadius: 4)
      ]),
      width: 300,
      child: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
                margin: EdgeInsets.zero,
                padding: EdgeInsets.zero,
                decoration: BoxDecoration(color: Colors.black),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (BuildContext context) => Container()));
                      },
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                            'https://pixel.nymag.com/imgs/daily/vulture/2017/06/14/14-tom-cruise.w700.h700.jpg'),
                        radius: 40.0,
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Khanyile.mas@gmail.com',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 14.0),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          'Siya Khanyile',
                          style: TextStyle(color: Colors.white, fontSize: 14.0),
                        ),
                      ],
                    )
                  ],
                )),
            // Divider(
            //   color: Colors.redAccent,
            // ),
            NavigationListItem(
                title: 'Home',
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (BuildContext context) => Container()));
                },
                icon: Icons.business_center_rounded),
            NavigationListItem(
              title: 'Reports',
              onPressed: () {},
              icon: Icons.business_center_rounded,
            ),
            NavigationListItem(
              title: 'Issues around me',
              onPressed: () {},
              icon: Icons.people,
            ),
            NavigationListItem(
              title: 'Support',
              onPressed: () {},
              icon: Icons.panorama_fisheye_rounded,
            ),
            NavigationListItem(
              title: 'Profile',
              onPressed: () {},
              icon: Icons.wallet_giftcard,
            ),
            Divider(
              thickness: 2,
            ),
            NavigationListItem(
              title: 'Terms of use',
              onPressed: () {},
              icon: Icons.notes_rounded,
            ),

            NavigationListItem(
              title: 'Sign out',
              onPressed: () {},
              icon: Icons.alternate_email,
            ),

            // ListTile(
            //   title: Text("Contact"),
            //   leading: IconButton(
            //     icon: Icon(Icons.contact_page),
            //     onPressed: () {},
            //   ),
            //   onTap: () {
            //     Navigator.of(context).pop();
            //     Navigator.of(context).push(MaterialPageRoute(
            //         builder: (BuildContext context) => ProfileScreen()));
            //   },
            // )
          ],
        ),
      ),
    );
  }
}

class NavigationListItem extends StatelessWidget {
  final String title;
  final Function onPressed;
  final IconData icon;

  const NavigationListItem({
    Key? key,
    required this.title,
    required this.onPressed,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      // onTap: onPressed,
      child: Container(
        // decoration: BoxDecoration(boxShadow: [
        //   BoxShadow(
        //     color: Theme.of(context).secondaryHeaderColor,
        //     blurRadius: 2,
        //   )
        // ]),
        child: ListTile(
          leading: Icon(
            icon,
            color: Constants.iconColor,
          ),
          title: Text(
            title,
            style: Theme.of(context).textTheme.subtitle1,
          ),
        ),
      ),
    );
  }
}
