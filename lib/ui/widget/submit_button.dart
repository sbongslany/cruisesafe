import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/material.dart';

class SubmitButton extends StatelessWidget {

  final String label;
  final VoidCallback onPressed;

  SubmitButton({
    required this.onPressed,
    required this.label
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
        onPressed: onPressed,
        child: Text(label, style: TextStyle(color: Constants.btnTextColor),),
        style: ElevatedButton.styleFrom(
          shape: StadiumBorder(),
          primary: Constants.btnColor,
          minimumSize: Size(double.infinity, 50), // double.infinity is the width and 30 is the height
        ),
    );
  }
}