import 'package:carousel_slider/carousel_controller.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cruisesafe/ui/widget/nav_drawer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarouselWithIndicator extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicator> {
  int _current = 0;
  final CarouselController _controller = CarouselController();

  final List<Widget> myData = [
    Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
            Text('Cruising Jo\'burg with ease',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Image.asset('assets/images/slide_1.jpg'),
          ],
        )),
    Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
            Text('Mobility made easy',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Image.asset('assets/images/slide_2.jpg'),
          ],
        )),
    Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
            Text('A Jo\'burg that works is a South Africa that works',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Image.asset('assets/images/slide_3.jpg'),
          ],
        )),
    Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
            Text('Always striving for service excellence',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Image.asset('assets/images/slide_4.jpg'),
          ],
        )),
    Container(
        height: 300,
        width: 300,
        child: Column(
          children: [
            Text('Serving Jo\'burg through actions',
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 14,
                    fontWeight: FontWeight.bold)),
            SizedBox(
              height: 5,
            ),
            Image.asset('assets/images/slide_5.jpg'),
          ],
        )),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white24,
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        SizedBox(
          height: 10,
        ),
        //Text("Cruising Jo'burg roads with ease", style: TextStyle(color: Colors.black,fontSize: 16,fontWeight: FontWeight.bold)),
        Flexible(
          child: CarouselSlider(
            items: myData,
            carouselController: _controller,
            options: CarouselOptions(
                height: 300,
                autoPlay: false,
                enlargeCenterPage: true,
                aspectRatio: 6.0,
                onPageChanged: (index, reason) {
                  setState(() {
                    _current = index;
                  });
                }),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: myData.asMap().entries.map((entry) {
            return GestureDetector(
              onTap: () => _controller.animateToPage(entry.key),
              child: Container(
                width: 12.0,
                height: 12.0,
                margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 4.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: (Theme.of(context).brightness == Brightness.dark
                            ? Colors.white
                            : Colors.black)
                        .withOpacity(_current == entry.key ? 0.9 : 0.4)),
              ),
            );
          }).toList(),
        ),
      ]),
    );
  }
}
