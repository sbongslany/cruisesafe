import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class RoundButton extends StatelessWidget {

  final String label;
  final String icon;
  final VoidCallback onPressed;
  final double buttonSize;
  final double iconWidth;


  RoundButton({
    required this.onPressed,
    required this.label,
    required this.icon,
    required this.buttonSize,
    required this.iconWidth,
  });

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: size.width * 0.4,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Stack(
              children: [
                Positioned(
                    child: Container(
                      height: size.height * 0.13,
                      width: size.height * 0.13,
                      decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.2),
                          shape: BoxShape.circle,
                          border: Border.all(width: 2, color: Colors.white),
                      ),
                    )
                ),
                Positioned(
                  left: 30,
                  top: 30,
                  right: 30,
                  bottom: 30,
                  child: Image.asset(icon, width: size.width * 0.003,),
                ),
              ],
            ),
            SizedBox(height: 5,),
            Text(label, style: TextStyle(fontSize: 14, color: Colors.white), textAlign: TextAlign.center,)
          ],
        ),
      ),
    );
  }
}