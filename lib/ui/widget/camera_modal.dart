import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CameraModal extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: const Text('Dialog Magic'),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height * 0.2,
        decoration: BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage("assets/images/home_background.jpeg"),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.3), BlendMode.dstATop),
          ),
        ),
        child: Column(
          children: [
            //Image.asset('assets/images/jra_splash2.png', width: MediaQuery.of(context).size.width * 0.4,),
            ListTile(
              onTap: () { Navigator.pop(context); /**_getFromCamera();*/ },
              leading: Icon(Icons.camera_alt_outlined, color: Constants.iconColor,),
              title: Text('Take a picture'),
            ),
            Expanded(child: Divider()),
            ListTile(
              onTap: () { Navigator.pop(context); /**_getFromGallery();*/ },
              leading: Icon(Icons.image, color: Constants.iconColor,),
              title: Text('Choose from Library'),
            ),
            ElevatedButton(
              onPressed: () { Navigator.pop(context); },
              child: Text('CANCEL', style: TextStyle(color: Constants.btnTextColor),),
              style: ElevatedButton.styleFrom(
                primary: Constants.btnColor,
                minimumSize: Size(double.infinity, MediaQuery.of(context).size.height * 0.06), // double.infinity is the width and 30 is the height
              ),
            ),
          ],
        ),
      ),
    );
  }
}