import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ExpansionList extends StatelessWidget {
  

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("FAQ'S"),
        ),
        body: ListView.separated(
          separatorBuilder: (context, index) => Divider(
            color: Colors.black,
          ),
          itemBuilder: (BuildContext context, int index) =>
              EntryItem(data[index]),
          itemCount: data.length,
        ),
      );
  }
}


class Entry {
  Entry(this.title, [this.children = const <Entry>[]]);

  final String title;
  final List<Entry> children;

}

final List<Entry> data =[
  Entry(
    'How do i log a service defect request',
    <Entry>[
      Entry(
        'You can report related faults here on the app \n\nOr call Joburg Connect '
            'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),

  Entry(
    'How long will it take to fix an issue',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
  Entry(
    'How do i clain for public Liability \n'
        '(damage to vehicle, property or\n'
        'personal)? ',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
  Entry(
    'What information should i provide when \n'
        'reporting / submitting a public liability\n'
        'claim for damage to vehicle, property or personal inquiry',
    <Entry>[
      Entry(
          'You can report related faults here on the app \n\nOr call Joburg Connect '
              'to\nCustomer Contact Centre: 0860 562 874 | Email\nhotline@jra.org.za'
      ),

    ],
  ),
];


class EntryItem extends StatelessWidget {
  const EntryItem(this.entry);

  final Entry entry;

  Widget _buildTiles(Entry root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return ExpansionTile(
      key: PageStorageKey<Entry>(root),
      title: Text(root.title, style: TextStyle(fontWeight: FontWeight.bold), ),
      children: root.children.map(_buildTiles).toList(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}