import 'package:cruisesafe/core/config/constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TopNav extends StatelessWidget {
  //const TopNav({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  void _openDrawer() {
    _scaffoldKey.currentState!.openDrawer();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        margin: EdgeInsets.only(bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              leading: IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Constants.iconColor,
                ),
                onPressed: () {
                  // Scaffold.of(context).openDrawer();
                  print('MENU CLICKED!!!');
                  //_openDrawer();
                  //_drawer(context);
                },
              ),
              title: Center(
                  child: Text(
                    'Cruise Safe',
                    style: TextStyle(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                        color: Constants.lblColor),
                  )),
              trailing: IconButton(
                icon: Icon(Icons.notifications, color: Constants.iconColor),
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Drawer _drawer(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            /**decoration: BoxDecoration(
                color: themeColor,
                image: DecorationImage(
                image: ExactAssetImage('assets/images/user_large.jpg'),
                fit: BoxFit.cover,
                ),
                ),*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //Image.asset("assets/images/user_large.jpg"),
                ListTile(
                  title: Text(
                    'Full Name',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 24,
                    ),
                  ),
                ),
              ],
            ),
          ),
          /**InkWell(
              onTap: (){
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => Membership()));
              },
              child: ListTile(
              leading: Icon(Icons.inbox, color: drawerIconColor,),
              title: Text('Inbox'),
              trailing: Icon(Icons.chevron_right),
              ),
              ),*/
          InkWell(
            onTap: () async {
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => ReceiptScreen()));

              await Navigator.of(context).push(new MaterialPageRoute<String>(
                  builder: (BuildContext context) {
                    //return ReceiptScreen();
                    return SizedBox();
                  },
                  fullscreenDialog: false
              ));
              //loadStats();

            },
            child: ListTile(
              leading: Icon(Icons.note, color: Constants.iconColor,),
              title: Text('Receipts'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          InkWell(
            onTap: () async {
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => VehicleLogbook()));
              await Navigator.of(context).push(new MaterialPageRoute<String>(
                  builder: (BuildContext context) {
                    //return VehicleLogbook();
                    return SizedBox();
                  },
                  fullscreenDialog: false
              ));
              //loadStats();
            },
            child: ListTile(
              leading: Icon(Icons.directions_car, color: Constants.iconColor,),
              title: Text('Vehicle Logbook'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          InkWell(
            onTap: (){
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => SettingsScreen()));
            },
            child: ListTile(
              leading: Icon(Icons.settings, color: Constants.iconColor,),
              title: Text('Settings'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          Divider(),
          ListTile(
            leading: Text("Other"),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => ContactUs()));
            },
            child: ListTile(
              leading: Icon(Icons.phone, color: Constants.iconColor,),
              title: Text('Help'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              //Navigator.push(context,MaterialPageRoute(builder: (context) => AboutUs()));
            },
            child: ListTile(
              leading: Icon(Icons.info_outline, color: Constants.iconColor,),
              title: Text('About'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              //logout();//clears all sessions
              //Navigator.pushReplacement(context,MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            child: ListTile(
              leading: Icon(Icons.add_to_home_screen, color: Constants.iconColor,),
              title: Text('Logout'),
              trailing: Icon(Icons.chevron_right),
            ),
          ),
        ],
      ),
    );
  }
}
